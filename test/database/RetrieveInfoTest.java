/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database;

import database.RetrieveInfo;
import java.sql.Blob;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import spring.domain.ExerciseGroup;
import spring.domain.User;


/**
 *
 * HUSK Ã… BRUKE LOKAL DB, OG DROPPE DB FÃ˜R TESTING
 */
public class RetrieveInfoTest {
	
	public RetrieveInfoTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
		database.DBConnection.makeConnection();
	}
	
	@AfterClass
	public static void tearDownClass() {
		database.DBConnection.closeResources();
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of getStudentListString method, of class RetrieveInfo.
	 * 
	 */
	@Test
	public void testGetStudentListString() {
		System.out.println("getStudentListString");
		ArrayList<String> expResult = new ArrayList();
		expResult.add("Abu, Dhabi, team2scrum@gmail.com");
		expResult.add("Nils, Iggun, nilg@gmail.com");
		expResult.add("Ole, Olsen, oo@gmail.com");
		expResult.add("Slow, Mo, slow@gmail.com");
		ArrayList<String> result = RetrieveInfo.getStudentListString();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getStudentListUser method, of class RetrieveInfo.
	 */
	//@Test
	public void testGetStudentListUser() {
		System.out.println("getStudentListUser");
		ArrayList<User> expResult = null;
		ArrayList<User> result = RetrieveInfo.getStudentListUser();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of getUserType method, of class RetrieveInfo.
	 */
	@Test
	public void testGetUserType() {
		System.out.println("getUserType");
		String email = "team2scrum@gmail.com";
		int expResult = 3;
		int result = RetrieveInfo.getUserType(email);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getUser method, of class RetrieveInfo.
	 */
	@Test
	public void testGetUser() {
		System.out.println("getUser");
		String email = "team2scrum@gmail.com";
		User expResult = new User();
		expResult.setfName("Abu");
		expResult.setlName("Dhabi");
		expResult.setEmail("team2scrum@gmail.com");
		expResult.setUser_type(3);
		User result = RetrieveInfo.getUser(email);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getSubjects method, of class RetrieveInfo.
	 */
	@Test
	public void testGetSubjects() {
		System.out.println("getSubjects");
		String studentMail = "team2scrum@gmail.com";
		String[] expResult = new String[]{"TDAT1000 Matte 1"};
		String[] result = RetrieveInfo.getSubjects(studentMail);
		assertArrayEquals(expResult, result);
	}
	
	/**
	 * Test of getSubjectCodes method, of class RetrieveInfo.
	 */
	@Test
	public void testGetSubjectCodes() {
		System.out.println("getSubjectCodes");
		String studentMail = "team2scrum@gmail.com";
		String[] expResult = new String[]{"TDAT1000"};
		String[] result = RetrieveInfo.getSubjectCodes(studentMail);
		assertArrayEquals(expResult, result);
	}

	/**
	 * Test of getSubjectList method, of class RetrieveInfo.
	 */
	@Test
	public void testGetSubjectList() {
		System.out.println("getSubjectList");
		String[] expResult = new String[]{"TDAT1000   Matte 1", "TDAT1001   Algdat", "TDAT1002   Systemutvikling 1"};
		String[] result = RetrieveInfo.getSubjectList();
		assertArrayEquals(expResult, result);
	}

	/**
	 * Test of isReadyForExam method, of class RetrieveInfo.
	 */
	@Test
	public void testIsReadyForExam() {
		System.out.println("isReadyForExam");
		String studentMail = "nilg@gmail.com";
		String subjectCode = "TDAT1001";
		boolean expResult = true;
		boolean result = RetrieveInfo.isReadyForExam(studentMail, subjectCode);
		assertEquals(expResult, result);
	}
	
	/**
	 * Test of isReadyForExam method, of class RetrieveInfo.
	 */
	@Test
	public void testIsReadyForExam2() {
		System.out.println("isReadyForExam");
		String studentMail = "nilg@gmail.com";
		String subjectCode = "TDAT1002";
		boolean expResult = false;
		boolean result = RetrieveInfo.isReadyForExam(studentMail, subjectCode);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getUserExercises method, of class RetrieveInfo.
	 */
	@Test
	public void testGetUserExercises() {
		System.out.println("getUserExercises");
		String studentMail = "nilg@gmail.com";
		String subjectCode = "TDAT1001";
		ArrayList<Integer> expResult = new ArrayList<Integer>();
		expResult.add(1);
		expResult.add(0);
		expResult.add(1);
		expResult.add(0);
		expResult.add(1);
		expResult.add(0);
		ArrayList<Integer> result = RetrieveInfo.getUserExercises(studentMail, subjectCode);
		assertEquals(expResult, result);
	}

	/**
	 * Test of existsInSubject method, of class RetrieveInfo.
	 */
	@Test
	public void testExistsInSubject() {
		System.out.println("existsInSubject");
		String studentMail = "nilg@gmail.com";
		String subjectCode = "TDAT1001";
		boolean expResult = true;
		boolean result = RetrieveInfo.existsInSubject(studentMail, subjectCode);
		assertEquals(expResult, result);
	}
	
	/**
	 * Test of existsInSubject method, of class RetrieveInfo.
	 */
	@Test
	public void testExistsInSubject2() {
		System.out.println("existsInSubject");
		String studentMail = "nilg@gmail.com";
		String subjectCode = "TDAT1000";
		boolean expResult = false;
		boolean result = RetrieveInfo.existsInSubject(studentMail, subjectCode);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getPassword method, of class RetrieveInfo.
	 */
	@Test
	public void testGetPassword() {
		System.out.println("getPassword");
		String email = "nilg@gmail.com";
		byte[] expResult = new byte[]{-107, 68};
		byte[] result = RetrieveInfo.getPassword(email);
		assertArrayEquals(expResult, result);
	}

	/**
	 * Test of getUnapprovedExercises method, of class RetrieveInfo.
	 */
	@Test
	public void testGetUnapprovedExercises() {
		System.out.println("getUnapprovedExercises");
		String studentMail = "nilg@gmail.com";
		String subjectCode = "TDAT1001";
		ArrayList<Integer> expResult = new ArrayList();
		expResult.add(2);
		expResult.add(4);
		expResult.add(6);
		ArrayList<Integer> result = RetrieveInfo.getUnapprovedExercises(studentMail, subjectCode);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getNumberOfExercises method, of class RetrieveInfo.
	 */
	@Test
	public void testGetNumberOfExercises() {
		System.out.println("getNumberOfExercises");
		String subjectCode = "TDAT1001";
		int expResult = 6;
		int result = RetrieveInfo.getNumberOfExercises(subjectCode);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getGroupIDs method, of class RetrieveInfo.
	 */
	@Test
	public void testGetGroupIDs() {
		System.out.println("getGroupIDs");
		String subjectCode = "TDAT1001";
		ArrayList<Integer> expResult = new ArrayList();
		expResult.add(0);
		expResult.add(1);
		expResult.add(2);
		ArrayList<Integer> result = RetrieveInfo.getGroupIDs(subjectCode);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getExerciseGroups method, of class RetrieveInfo.
	 */
	@Test
	public void testGetExerciseGroups() {
		System.out.println("getExerciseGroups");
		String subjectCode = "TDAT1001";
		ArrayList<ExerciseGroup> expResult = new ArrayList();
		ExerciseGroup a = new ExerciseGroup();
		ArrayList<Integer> b = new ArrayList();
		b.add(1);
		b.add(2);
		a.setExercises(b);
		a.setSubjectCode("TDAT1001");
		a.setMandatory(1);
		a.setGroupID(0);
		expResult.add(a);
		
		a = new ExerciseGroup();
		b = new ArrayList();
		b.add(3);
		b.add(4);
		a.setExercises(b);
		a.setSubjectCode("TDAT1001");
		a.setMandatory(1);
		a.setGroupID(1);
		expResult.add(a);
		
		a = new ExerciseGroup();
		b = new ArrayList();
		b.add(5);
		b.add(6);
		a.setExercises(b);
		a.setSubjectCode("TDAT1001");
		a.setMandatory(1);
		a.setGroupID(2);
		expResult.add(a);
		ArrayList<ExerciseGroup> result = RetrieveInfo.getExerciseGroups(subjectCode);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getExercisesFromGroup method, of class RetrieveInfo.
	 */
	@Test
	public void testGetExercisesFromGroup() {
		System.out.println("getExercisesFromGroup");
		String subjectCode = "TDAT1001";
		int groupID = 0;
		ArrayList<Integer> expResult = new ArrayList();
		expResult.add(1);
		expResult.add(2);
		ArrayList<Integer> result = RetrieveInfo.getExercisesFromGroup(subjectCode, groupID);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getStudentsFromSubject method, of class RetrieveInfo.
	 */
	@Test
	public void testGetStudentsFromSubject() {
		System.out.println("getStudentsFromSubject");
		String subjectCode = "TDAT1001";
		String[] expResult = new String[] {"nilg@gmail.com, Iggun, Nils, Godkjent", "oo@gmail.com, Olsen, Ole, Ikke godkjent"};
		String[] result = RetrieveInfo.getStudentsFromSubject(subjectCode);
		assertArrayEquals(expResult, result);
	}
	
}
