/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database;

import database.ValidateInfo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bjox
 */
public class ControlTest {
	
	public ControlTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
		database.DBConnection.makeConnection();
	}
	
	@AfterClass
	public static void tearDownClass() {
		database.DBConnection.closeResources();
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of userExist method, of class Control.
	 */
	@Test
	public void testUserExist() {
		System.out.println("userExist");
		String email = "team2scrum@gmail.com";
		boolean expResult = true;
		boolean result = ValidateInfo.userExist(email);
		assertEquals(expResult, result);
	}
	
	/**
	 * Test of userExist method, of class Control.
	 */
	@Test
	public void testUserExist2() {
		System.out.println("userExist");
		String email = "javsa@gmail.com";
		boolean expResult = false;
		boolean result = ValidateInfo.userExist(email);
		assertEquals(expResult, result);
	}

	/**
	 * Test of simpleWord method, of class Control.
	 */
	@Test
	public void testSimpleWord() {
		System.out.println("simpleWord");
		String word = "sykkel";
		boolean expResult = true;
		boolean result = ValidateInfo.isSimpleWord(word);
		assertEquals(expResult, result);
	}
	
	/**
	 * Test of simpleWord method, of class Control.
	 */
	@Test
	public void testSimpleWord2() {
		System.out.println("simpleWord");
		String word = "bjek3h";
		boolean expResult = false;
		boolean result = ValidateInfo.isSimpleWord(word);
		assertEquals(expResult, result);
	}
	
}
