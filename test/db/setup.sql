
-- DROPPING TABLES
drop table Groups;
drop table Requirements;
drop table Exercises;
drop table Users_subjects;
drop table Subjects;
drop table Users;


-- CRATING TABLES
create table Users (
	first_name varchar(127) not null,
	last_name varchar(127) not null,
	email varchar(127) not null,
	password blob(1K) not null,
	user_type integer not null, -- 0:admin, 1:teacher, 2:helper, 3:student 
	constraint users_pk primary key (email)
);

create table Subjects (
	subject_code varchar(31) not null,
	subject_name varchar(127) not null,
	minimum_req integer not null,
	number_of_exercises integer not null,
	professor varchar(127) not null,
	constraint subjects_pk primary key (subject_code)
);

create table Users_subjects (
	user_email varchar(127) not null,
	subject_code varchar(31) not null,
	constraint users_subjects_pk primary key (user_email, subject_code)
);

create table Exercises (
	user_email varchar(127) not null,
	subject_code varchar(31) not null,
	exercise integer not null,
	approved integer not null,
	constraint exercises_pk primary key (user_email, subject_code, exercise)
);

create table Requirements (
	subject_code varchar(31) not null,
	ex_group integer not null,
	num_mandatory integer not null,
	constraint requirements_pk primary key (subject_code, ex_group)
);

create table Groups (
	subject_code varchar(31) not null,
	exercise integer not null,
	ex_group integer not null,
	constraint groups_pk primary key (subject_code, exercise)
);

-- ADDING CONSTRAINTS
alter table users add constraint user_type_check check(user_type between 0 and 3);


-- ADDING FOREIGN KEYS
alter table Users_subjects add constraint email_fk foreign key(user_email) references Users(email);
alter table Users_subjects add constraint subject_code_fk foreign key(subject_code) references Subjects(subject_code);

alter table Exercises add constraint email_fk2 foreign key(user_email) references Users(email);
alter table Exercises add constraint subject_code_fk2 foreign key(subject_code) references Subjects(subject_code);

alter table Groups add constraint subject_code_fk3 foreign key(subject_code) references Subjects(subject_code);
alter table Groups add constraint ex_group_fk foreign key(subject_code, ex_group)
	references Requirements(subject_code, ex_group);

alter table Subjects add constraint email_fk3 foreign key(professor) references Users(email);