-- first_name, last_name, email, password, user_type --> 0:admin, 1:teacher/helper, 2:student 
insert into Users values ('Abu','Dhabi','team2scrum@gmail.com', cast ( X'09AF44' as blob(1K) ) ,3);
insert into Users values ('Nils','Iggun','nilg@gmail.com', cast ( X'9544' as blob(1K) ) ,3);
insert into Users values ('Ole','Olsen','oo@gmail.com', cast ( X'4563' as blob(1K) ) ,3);
insert into Users values ('Slow','Mo','slow@gmail.com', cast ( X'5354' as blob(1K) ) ,3);

-- subject_code, subject_name, minimum_req, num_of_exercises, professor
insert into Subjects values ('TDAT1000','Matte 1', 0, 6, 'slow@gmail.com');
insert into Subjects values ('TDAT1001','Algdat', 3, 6, 'slow@gmail.com');
insert into Subjects values ('TDAT1002','Systemutvikling 1', 8, 5, 'slow@gmail.com');

-- user_email, subject_code
insert into Users_subjects values ('team2scrum@gmail.com','TDAT1000');
insert into Users_subjects values ('nilg@gmail.com','TDAT1001');
insert into Users_subjects values ('nilg@gmail.com','TDAT1002');
insert into Users_subjects values ('oo@gmail.com','TDAT1001');

-- user_email, subject_code, exercise nr, approved --> 0:not, 1:approved
insert into Exercises values ('nilg@gmail.com','TDAT1001',1,1);
insert into Exercises values ('nilg@gmail.com','TDAT1001',2,0);
insert into Exercises values ('nilg@gmail.com','TDAT1001',3,1);
insert into Exercises values ('nilg@gmail.com','TDAT1001',4,0);
insert into Exercises values ('nilg@gmail.com','TDAT1001',5,1);
insert into Exercises values ('nilg@gmail.com','TDAT1001',6,0);

insert into Exercises values ('oo@gmail.com','TDAT1001',1,0);
insert into Exercises values ('oo@gmail.com','TDAT1001',2,0);
insert into Exercises values ('oo@gmail.com','TDAT1001',3,0);
insert into Exercises values ('oo@gmail.com','TDAT1001',4,0);
insert into Exercises values ('oo@gmail.com','TDAT1001',5,0);
insert into Exercises values ('oo@gmail.com','TDAT1001',6,0);

insert into Exercises values ('nilg@gmail.com','TDAT1002',1,1);
insert into Exercises values ('nilg@gmail.com','TDAT1002',2,1);
insert into Exercises values ('nilg@gmail.com','TDAT1002',3,1);
insert into Exercises values ('nilg@gmail.com','TDAT1002',4,1);
insert into Exercises values ('nilg@gmail.com','TDAT1002',5,0);
insert into Exercises values ('nilg@gmail.com','TDAT1002',6,1);
insert into Exercises values ('nilg@gmail.com','TDAT1002',7,1);
insert into Exercises values ('nilg@gmail.com','TDAT1002',8,0);
insert into Exercises values ('nilg@gmail.com','TDAT1002',9,0);
insert into Exercises values ('nilg@gmail.com','TDAT1002',10,1);


-- subject_code, ex_group, num_mandatory
insert into Requirements values ('TDAT1002', 0, 2);
insert into Requirements values ('TDAT1002', 1, 2);
insert into Requirements values ('TDAT1002', 2, 2);
insert into Requirements values ('TDAT1002', 3, 1);

insert into Requirements values ('TDAT1001', 0, 1);
insert into Requirements values ('TDAT1001', 1, 1);
insert into Requirements values ('TDAT1001', 2, 1);

-- subject_code, exercise, ex_group
insert into Groups values ('TDAT1002', 1, 0);
insert into Groups values ('TDAT1002', 2, 0);
insert into Groups values ('TDAT1002', 3, 1);
insert into Groups values ('TDAT1002', 4, 1);
insert into Groups values ('TDAT1002', 5, 1);
insert into Groups values ('TDAT1002', 6, 2);
insert into Groups values ('TDAT1002', 7, 2);
insert into Groups values ('TDAT1002', 8, 3);
insert into Groups values ('TDAT1002', 9, 3);
insert into Groups values ('TDAT1002', 10, 3);

insert into Groups values ('TDAT1001', 1, 0);
insert into Groups values ('TDAT1001', 2, 0);
insert into Groups values ('TDAT1001', 3, 1);
insert into Groups values ('TDAT1001', 4, 1);
insert into Groups values ('TDAT1001', 5, 2);
insert into Groups values ('TDAT1001', 6, 2);
