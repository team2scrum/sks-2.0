package spring.controller;

import database.DBConnection;
import database.EditInfo;
import database.RetrieveInfo;
import javax.servlet.http.HttpServletRequest;
import spring.domain.User;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import business.UserCreator;
import java.util.Date;
import business.Encryption;
import business.FileCreator;
import business.Queue;
import business.QueueContainer;
import business.QueueElement;
import business.UserElement;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import spring.domain.ExerciseGroup;
import spring.domain.UserList;

@Controller
public class SksController {

    private final Encryption enc = new Encryption();
    private QueueContainer qc = new QueueContainer();

    /**
     * controller to all requst(URL), for those @RequestMapping() that is not
     * specified below.
     *
     * @param user
     * @param request
     * @return to login screen
     */
    @RequestMapping("/*")
    public String showStartView(@ModelAttribute(value = "userInfo") User user, HttpServletRequest request) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        DBConnection.makeConnection(); // Connection to database
        request.getSession().setAttribute("user", new User());

        return "/WEB-INF/jsp/login.jsp";
        // </editor-fold>
    }

    /**
     * Specified @RequestMapping("checkLogin"), operate request from
     * checkLogin.htm returns to login if password or username is incorrect
     *
     * @param user
     * @param userTest
     * @param error
     * @param request
     * @return mainscreen if authentication succeeds
     */
    @RequestMapping("checkLogin")
    public String validateLogin(
            @Valid @ModelAttribute(value = "userInfo") User user,
            @ModelAttribute(value = "userTest") User userTest, BindingResult error, HttpServletRequest request) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">

        if (error.hasErrors()) {
            return "WEB-INF/jsp/login.jsp"; //return to login page if errors
        }
        if (userTest != null) {
            System.out.println("userTest != null");
            System.out.println(userTest.toString());
        }
        User userSession = (User) request.getSession().getAttribute("user"); // creating session

        if (user.getEmail() != null) {
            userSession.setCryptPass(user.getCryptPass());
            userSession.setEmail(user.getEmail());
        }
        String email = userSession.getEmail();

        //Check user and pw with database
        byte[] check = RetrieveInfo.getPassword(email);
        if (java.util.Arrays.equals(check, userSession.getCryptPass())) {
            userSession = RetrieveInfo.getUser(userSession.getEmail());
            userSession.setUser_type(RetrieveInfo.getUserType(email));
            userSession.setSubjects(RetrieveInfo.getSubjects(userSession.getEmail()));

            user.setfName(userSession.getfName());
            user.setlName(userSession.getlName());
            user.setSubjects(userSession.getSubjects());
            user.setUser_type(userSession.getUser_type());

            request.getSession().setAttribute("user", userSession);

            return "WEB-INF/jsp/changePw.jsp"; // logs you in to mainscreen if auth succeeds
        }
        return "WEB-INF/jsp/login.jsp"; // return to login if pw/user incorrect
        // </editor-fold>
    }

    /**
     * Specified @RequestMapping("addUser"), operate request from addUser.htm
     * return to addUser screen if user is not added
     *
     * @param user
     * @param uc
     * @param userSession
     * @param request
     * @return to addUser screen if user is added
     */
    @RequestMapping("addUser")
    public String addUser(@Valid @ModelAttribute(value = "userInfo") User user,
            @ModelAttribute(value = "userText") UserCreator uc,
            @ModelAttribute(value = "userSession") User userSession, HttpServletRequest request) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">

        User userFromSessionObj = (User) request.getSession().getAttribute("user"); //creating session

		//****Copying the variables from testUser to admin is needed!!!!****
        user.setfName(userFromSessionObj.getfName());
        user.setlName(userFromSessionObj.getlName());
        user.setUser_type(userFromSessionObj.getUser_type());

        userSession.setEmail(userFromSessionObj.getEmail());
        userSession.setUser_type(userFromSessionObj.getUser_type());
        userSession.setfName(userFromSessionObj.getfName());
        userSession.setlName(userFromSessionObj.getlName());
        userSession.setSubjects(RetrieveInfo.getSubjects(userSession.getEmail()));

        // Creating a date obj to visualy see how many milisec it takes to send an eMail
        Date start = new Date();
        double time;
        Date slutt;
        if (user.getEmail() == null) {
            if (uc.getText() != null) {
                uc.createUsers();
                slutt = new Date();
                time = (double) (slutt.getTime() - start.getTime());
                System.out.println("Milisekund tid " + time); //printing milisec elapsed time
                return "WEB-INF/jsp/addUser.jsp";
            }
        } else {
            uc.createUser(user, user.getSubjects()[0]);
            return "WEB-INF/jsp/addUser.jsp";
        }
        return "WEB-INF/jsp/addUser.jsp";
        // </editor-fold>
    }

    /**
     * Specified @RequestMapping("changePw"), operate request from changePw.htm
     * return to chagePassword screen if the old passwords does not equal to
     * eachother, or the newPassword1 is not equal to newPassword2
     *
     * @param request
     * @param user
     * @return to changePassword screen if succeeds
     */
    @RequestMapping("changePw")
    public String changePw(HttpServletRequest request, @ModelAttribute(value = "userInfo") User user) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        System.out.println("CHANGE PASSWORD!!!!! ");
        String oldPw = request.getParameter("oldPassword");
        String newPw1 = request.getParameter("newPassword1");
        String newPw2 = request.getParameter("newPassword2");

        User userSession = (User) request.getSession().getAttribute("user"); // creating session

        user.setfName(userSession.getfName());
        user.setlName(userSession.getlName());
        user.setSubjects(userSession.getSubjects());
        user.setUser_type(userSession.getUser_type());
        user.setEmail(userSession.getEmail());
        if (oldPw != null
                && newPw1.equals(newPw2)
                && !java.util.Arrays.equals(user.getCryptPass(),
                        enc.encrypt(oldPw))) {
            EditInfo.changePassword(user.getEmail(), enc.encrypt(newPw1));
            user.setCryptPass(enc.encrypt(newPw1));
            request.getSession().setAttribute("user", user);

            return "WEB-INF/jsp/changePw.jsp";
        }
        return "WEB-INF/jsp/changePw.jsp";
        // </editor-fold>
    }

    /**
     * Specified @RequestMapping("logout"), operate request from logout.htm the
     * login button may be clicked from all screens
     *
     * @param user
     * @param request
     * @return to login screen if button "logout" is clicked
     */
    @RequestMapping("logout")
    public String logout(@ModelAttribute(value = "userInfo") User user, HttpServletRequest request) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        request.getSession().setAttribute("user", new User());
        return "WEB-INF/jsp/login.jsp";
        // </editor-fold>
    }

    /**
     * Specified @RequestMapping("queue"), operate request from queue.htm shows
     * all queue elements in the selected subject
     *
     * @param user
     * @param subjectName
     * @param request
     * @param tablestring
     * @return to queue screen
     */
    @RequestMapping("queue")
    public String queue(@ModelAttribute(value = "userInfo") User user, @ModelAttribute(value = "queue") String tablestring, HttpServletRequest request, String subjectName) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        User userSession = (User) request.getSession().getAttribute("user"); // creating session
        int userType = userSession.getUser_type();
        if (subjectName == null) {
            try {
                subjectName = (String) request.getSession().getAttribute("selectedValue");
                tablestring = getQueueTable((String) request.getSession().getAttribute("selectedValue"), userType, userSession);
                System.out.println("tablestring: " + tablestring);
            } catch (Exception e) {
                System.out.println("e");
            }
        } else {
            tablestring = getQueueTable(subjectName, userType, userSession);
        }
        String queueButtons = "";
        String getInQueue = "";
        System.out.println("userType: " + userType + "\nSubjectName = null?: " + (subjectName == null) + "\nSubjectName: " + subjectName + "\nqc.getQueue: " + qc.getQueue(subjectName));
        if (subjectName == null) {
            queueButtons = "";
        } else if (userType == 3) {
            if (qc.getQueue(subjectName) == null) {
                queueButtons = "";
            } else if (qc.getQueue(subjectName).getQueueState() == 0) {
                queueButtons = "";
            } else {
                boolean showQueueButton = true;
                QueueElement[] quElem = qc.getQueue(subjectName).getElements();
                for (int i = 0; i < quElem.length; i++) {
                    UserElement[] users = quElem[i].getAllUsers();
                    for (int j = 0; j < users.length; j++) {
                        if (users[j].getEmail().equals(userSession.getEmail())) {
                            showQueueButton = false;
                            getInQueue = "";
                            break;
                        }
                        if (!showQueueButton) {
                            break;
                        }
                    }
                }
                if (showQueueButton) {
                    getInQueue = "<button name=\"changeQueueState\" class=\"btn-primary\" value = \"5\" type=\"submit\">Still deg i kø</button>";
                }
            }
        } else if (qc.getQueue(subjectName) == null || qc.getQueue(subjectName).getQueueState() == 0) {
            queueButtons = "<button name=\"changeQueueState\" class=\"btn-primary\" value=\"1\" type=\"submit\">Start køen</button>";
        } else {
            queueButtons = "<button name=\"changeQueueState\" class=\"btn-primary\" value=\"0\" type=\"submit\">Avslutt køen</button>"
                    + "<button name=\"changeQueueState\" class=\"btn-primary\" value=\"2\" type=\"submit\">Steng køen</button>";
        }

        user.setfName(userSession.getfName());
        user.setlName(userSession.getlName());
        user.setSubjects(userSession.getSubjects());
        user.setUser_type(userSession.getUser_type());
        request.getSession().setAttribute("queueTable", tablestring);
        request.getSession().setAttribute("queueButtons", queueButtons);
        request.getSession().setAttribute("getInQueue", getInQueue);
        return "/WEB-INF/jsp/queue.jsp";
        // </editor-fold>
    }

    /**
     * Returns a string with the html code with all the elements in the elements
     * in the selected subject
     *
     * @param subjectName The selected subject to get elements from
     * @param userType
     * @param user
     * @return
     */
    public String getQueueTable(String subjectName, int userType, User user) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        String answer = "";
        Queue queue = qc.getQueue(subjectName);
        System.out.println(subjectName);
        if (queue == null && subjectName == null) {
            return "<tr><td>Velg et fag for å se øvingskøen</td></tr>";
        }
        if (queue == null) {
            answer += "<tr><td>Køa har ikke startet</td></tr>";
            return answer;
        }
        QueueElement[] qe = queue.getElements();
        if (queue.getQueueState() == 0 && qe.length == 0) {
            answer += "<tr><td>Køa har ikke startet</td></tr>";
            return answer;
        } else if (queue.getQueueState() == 1 && qe.length == 0) {
            answer += "<tr><td>Ingen står i køen</td></tr>";

        }

        for (int i = 0; i < qe.length; i++) {

            if (qe[i].getIsHelped()) {
                answer += "<tr style=\"background-color:#81FF75\">";
            } else {
                answer += "<tr>";
            }
            answer += "<td id=\"td1\">" + (i + 1) + "</td>" + "<td id=\"td2\">" + qe[i].getUser().getName();
            if (qe[i].isStudentGroup()) {
                answer += " med gruppe";
            } else {

            }
            answer += "</td><td id=\"td3\">";
            int[] ex = qe[i].getExercises();
            for (int j = 0; j < ex.length; j++) {
                answer += ex[j] + ", ";
            }
            if (qe[i].getComment() != null || !qe[i].getComment().equals("")) {
                answer += "&nbsp;&nbsp;&nbsp;&nbsp;Kommentar: " + qe[i].getComment();
            }
            answer += "<br>Sitter ved " + qe[i].getPlacement() + "</td><td id=\"td4\">" + qe[i].getTimeInQueue() + " min</td><td id=\"td5\">";
            if (userType != 3) {
                if (qe[i].getIsHelped()) {
                    answer += "<button class=\"btn-primary queueButton\" data-toggle='collapse'\" type=\"button\" onclick='toggle(" + i + ")' style=\"text-align: left;\" name=\"qe\" value=\"" + i + "\"/>GODKJENN";
                    answer += "</button><button class=\"btn-alert queueButton\" style=\"text-align: left;\" name=\"qe\" value=" + (1000 + i) + ">FJERN</button><button class=\"btn-alert queueButton\" style=\"text-align: left;\" name=\"qe\" value=" + (2000 + i) + ">UTSETT</button>";
                } else {
                    answer += "<button class=\"btn-alert queueButton\" data-toggle='collapse'\" style=\"text-align: left;\" name=\"qe\" value=\"" + i + "\"/>HJELP";

                    answer += "</button><button class=\"btn-alert queueButton\" style=\"text-align: left;\" name=\"qe\" value=" + (1000 + i) + ">FJERN</button><button class=\"btn-alert queueButton\" style=\"text-align: left;\" name=\"qe\" value=" + (2000 + i) + ">UTSETT</button>";
                }
            } else if (qe[i].getUser().getEmail().equals(user.getEmail())) {
                answer += "<button class=\"btn-alert queueButton\" style=\"text-align: left;\" name=\"qe\" value=" + (1000 + i) + ">FJERN</button>";
            }
            answer += "</td></tr><tr id=\"tr" + i + "\" name='trName' style='display:none'><td id=\"td1\"></td><td id=\"td2\"></td><td id=\"td3\">";
            for (int j = 0; j < ex.length; j++) {
                answer += "Oving " + ex[j] + "&nbsp;<input type='checkbox' name='ovingcheck' value='" + ex[j] + "' class='check" + i + "'>&nbsp;&nbsp;&nbsp;";
            }
            answer += "</td><td id=\"td4\"></td><td id=\"td5\"></td></tr>";

        }
        return answer;
        // </editor-fold>
    }

    /**
     * Specified @RequestMapping("deleteTable"), operate request from queue.htm
     * On button click from queue.htm, either set isHelped, approves exercises
     * or removes queue, depending on which button is clicked from queue.htm
     *
     * @param a
     * @param user
     * @param error
     * @param request
     * @return
     */
    @RequestMapping("deleteTable")
    public String removeQueue(@RequestParam("qe") int a, @ModelAttribute("userInfo") User user, BindingResult error, HttpServletRequest request) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        String subjectName = (String) request.getSession().getAttribute("selectedValue");
        //Check if user is studass, teacher or admin
        if (a < 1000) {
            
            if (request.getParameterValues("ovingcheck") == null) {
                qc.getQueue(subjectName).getIndexElement(a).setIsHelped(true);
                return queue(user, "", request, null);
            } else {
                
                String[] exercisesString = request.getParameterValues("ovingcheck");
                int[] exercises = new int[exercisesString.length];
                for(int i = 0;i<exercises.length;i++){
                    exercises[i] = Integer.parseInt(exercisesString[i]);
                
            }
            if (qc.getQueue(subjectName).getIndexElement(a).getIsHelped()) {
                EditInfo insert = new EditInfo();
                UserElement[] users = qc.getQueue(subjectName).getIndexElement(a).getAllUsers();
                for (int i = 0; i < users.length; i++) {
                    boolean didItWork = insert.approveExercise(users[i].getEmail(), subjectName, exercises);
                }
                qc.getQueue(subjectName).remove(qc.getQueue(subjectName).getIndexElement(a));
            
            }
            }
        } else if (a < 2000) {
            a -= 1000;

            qc.getQueue(subjectName).remove(qc.getQueue(subjectName).getIndexElement(a));
        } else {
            a -= 2000;
            //set utset
        }

        //qc.getQueue("hi").getIndexElement(a).setIsHelped(true);   
        String ab = "";
        return queue(user, ab, request, null);
        // </editor-fold>

    }

    /**
     * Specified @RequestMapping("changeDrowdown"), operate request from
     * queue.htm Changes which subject is selected, depending from which subject
     * the user selected from the dropdown in queue.htm
     *
     * @param user
     * @param error
     * @param request
     * @return
     */
    @RequestMapping("changeDropdown")
    public String changeDropDown(@ModelAttribute("userInfo") User user, BindingResult error, HttpServletRequest request) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        String selectedSubject = request.getParameter("selectedValue");
        System.out.println(selectedSubject);
        request.getSession().setAttribute("selectedValue", selectedSubject);
        return queue(user, "", request, selectedSubject);
        // </editor-fold>

    }

    /**
     * Specified @RequestMapping("changeQueueState"), operate request from
     * queue.htm Changes the queue state in the selected subject or redirects to
     * getInQueue page, depending on which button is clicked in queue.htm.
     *
     * @param user
     * @param error
     * @param request
     * @return
     */
    @RequestMapping("changeQueueState")
    public String changeQueueState(@ModelAttribute("userInfo") User user, BindingResult error, HttpServletRequest request) {
       // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        //Which button is clicked? 1 = startQueue, 0 = end queue, 5 = insertIntoQueue
        String getValue = request.getParameter("changeQueueState");
        //Get which subject to change a state
        String selectedSubject = (String) request.getSession().getAttribute("selectedValue");

        if (getValue.equals("0")) {
            qc.getQueue(selectedSubject).setQueueState(0);
        } else if (getValue.equals("1")) {
            if (qc.getQueue(selectedSubject) == null) {
                Queue queue = new Queue(1, selectedSubject);
                qc.addQueue(selectedSubject, queue);
                System.out.println(qc.getQueue(selectedSubject).getSubjectCode());
            } else {
                qc.getQueue(selectedSubject).setQueueState(1);
            }
        } else if (getValue.equals("5")) {
            return getInQueue(new QueueElement(), error, user, request);
        }

        return queue(user, "", request, selectedSubject);
        // </editor-fold>
    }

    /**
     * Specified @RequestMapping("mainScreen"), operate request from login.htm
     * Shows the main stream after login.
     *
     * @param user
     * @param request
     * @return
     */
    @RequestMapping("mainScreen")
    public String mainScreen(@ModelAttribute(value = "userInfo") User user, HttpServletRequest request) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        User userSession = (User) request.getSession().getAttribute("user"); // creating session

        user.setfName(userSession.getfName());
        user.setlName(userSession.getlName());
        user.setSubjects(userSession.getSubjects());
        user.setUser_type(userSession.getUser_type());

        return "/WEB-INF/jsp/mainScreen.jsp";
        // </editor-fold>
    }

    /**
     * Specified @RequestMapping("exercises"), operate request from any page
     * Redirects to exercises.htm, and shows which exercise is done and which is
     * not
     *
     * @param user
     * @param userList
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("exercises")
    public String exercises(@ModelAttribute(value = "userInfo") User user,
            @ModelAttribute(value = "userList") UserList userList,
            HttpServletRequest request, HttpServletResponse response) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">

        User userSession = (User) request.getSession().getAttribute("user"); // creating session

        user.setEmail(userSession.getEmail());
        user.setfName(userSession.getfName());
        user.setlName(userSession.getlName());
        user.setSubjects(RetrieveInfo.getSubjects(userSession.getEmail()));
        user.setUser_type(userSession.getUser_type());

        System.out.println(user.getEmail());

        String subjectSelected = request.getParameter("subjectSelected");
        if (subjectSelected != null) {
            userList.setUserList(RetrieveInfo.getStudentsFromSubject(subjectSelected));
        }

        String buttonClicked = request.getParameter("buttonClicked");
        if (buttonClicked != null) {
            if (buttonClicked.equals("true")) {
                System.out.println(buttonClicked);
                FileCreator.createFile(userList.getUserList(), subjectSelected + ".txt");
                try {
                    File downloadFile = new File(subjectSelected + ".txt");
                    InputStream inputStream = new FileInputStream(downloadFile);
                    response.setContentType("application/force-download");
                    response.setHeader("Content-disposition", "attachment; filename=" + subjectSelected + ".txt");
                    IOUtils.copy(inputStream, response.getOutputStream());
                    response.flushBuffer();
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }

        request.getSession().setAttribute("user", user);

        return "WEB-INF/jsp/exercises.jsp";
        // </editor-fold>
    }

    /**
     * Not in use
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("exerciseRequirements")
    public String exerciseRequirements(HttpServletRequest request, HttpServletResponse response) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        String[] exSelectedString = request.getParameterValues("exercisesSelected");
        if (exSelectedString != null) {

            ArrayList<ExerciseGroup> exGroups;
            exGroups = (ArrayList) request.getSession().getAttribute("exReq");
            if (exGroups == null) {
                System.out.println("exGroups == null, prøver å hente fra RetrieveInfo");
                exGroups = RetrieveInfo.getExerciseGroups(request.getParameter("subjectSelected"));
                if (exGroups == null) {
                    System.out.println("exGroups fortsatt == null, lager nytt objekt");
                    exGroups = new ArrayList<ExerciseGroup>();
                }
            }
            if (request.getParameter("changeSubjectCheck").equals("true")) {
                exGroups = RetrieveInfo.getExerciseGroups(request.getParameter("subjectSelected"));
                if (exGroups == null) {
                    exGroups = new ArrayList<ExerciseGroup>();
                }
            }
            //If  exercises have been selected, it creates a list of the exercises and adds them to a new requirement group
            System.out.println("exSelectedString!=null. Length = " + exSelectedString.length);
            int[] exSelectedInt = new int[exSelectedString.length];
            for (int i = 0; i < exSelectedString.length; i++) {
                exSelectedInt[i] = Integer.parseInt(exSelectedString[i]);
                System.out.println("exSelectedInt " + exSelectedInt[i]);
            }
            ExerciseGroup newGroup = new ExerciseGroup();
            newGroup.setSubjectCode(request.getParameter("subjectSelected"));
            for (int i = 0; i < exSelectedInt.length; i++) {
                newGroup.addExercise(exSelectedInt[i]);
            }

			//Creates a list of requirements for the subject
            if (!exerciseExists(exGroups, newGroup)) {
                exGroups.add(newGroup);
            } else if (exerciseExists(exGroups, newGroup)) {
                //print errormessage TO DO
            }

            for (ExerciseGroup aGroup : exGroups) {
                System.out.println(aGroup.getSubjectCode());
                for (int anInt : aGroup.getExercises()) {
                    System.out.println(anInt);
                }
            }

            request.getSession().setAttribute("exReq", exGroups);
        }
        return "WEB-INF/jsp/exerciseRequirements.jsp";
        // </editor-fold>
    }

    /**
     *
     * Checking if an exercise exists in another group in the subject, return
     * true if the exercise already exists
     *
     * @param exGrp
     * @param newGroup
     * @return
     */
    public boolean exerciseExists(ArrayList<ExerciseGroup> exGrp, ExerciseGroup newGroup) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        for (ExerciseGroup aGroup : exGrp) {
            for (int exercise : newGroup.getExercises()) {
                if (aGroup.getExercises().contains(exercise)) {
                    return true;
                }
            }
        }

        return false;
        // </editor-fold>
    }

    /**
     * Specified @RequestMapping("queueUp"), operate request from queueUp.htm
     * Add an UserElement to the queue and redirects to queue.htm
     *
     * @param qe
     * @param user
     * @param request
     * @param error
     * @return
     */
    @RequestMapping("queueUp")
    public String queueUp(@ModelAttribute(value = "queueElement") QueueElement qe, @ModelAttribute(value = "userInfo") User user, HttpServletRequest request, BindingResult error) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">

        User userSession = (User) request.getSession().getAttribute("user");
        user.setEmail(userSession.getEmail());
        user.setfName(userSession.getfName());
        user.setlName(userSession.getlName());
        user.setSubjects(RetrieveInfo.getSubjects(userSession.getEmail()));
        user.setUser_type(userSession.getUser_type());

        User userToQueue = RetrieveInfo.getUser(userSession.getEmail());
        String table = request.getParameter("table");
        String[] exercisesString = request.getParameterValues("exercises");
        QueueElement[] qeElem = qc.getQueue((String) request.getSession().getAttribute("selectedValue")).getElements();
        for (int i = 0; i < qeElem.length; i++) {
            UserElement[] users = qeElem[i].getAllUsers();
            for (int j = 0; j < users.length; j++) {
                if (user.getEmail().equals(users[j].getEmail())) {
                    return queue(user, "", request, null);
                }
            }
        }
        int[] exercisesInt = new int[exercisesString.length];
        for (int i = 0; i < exercisesInt.length; i++) {
            exercisesInt[i] = Integer.parseInt(exercisesString[i]);
        }
        qe.setTable(table);
        qe.setExercises(exercisesInt);
        QueueElement qeNew = new QueueElement(new UserElement(userToQueue.getfName(), userToQueue.getlName(), userToQueue.getEmail()),
                qe.getFloor(), qe.getTable(), qe.getComment(), qe.getExercises());
        qc.getQueue((String) request.getSession().getAttribute("selectedValue")).add(qeNew);
        request.getSession().setAttribute("user", user);
        System.out.println("queueUp fname: " + user.getfName());
        return queue(user, "", request, null);
        // </editor-fold>
    }

    /**
     * Specified @RequestMapping("getInQueue), operate request from queue.htm
     * Redirects to queueUp.htm, which is a page for the user to add himself
     * into the queue
     *
     * @param qe
     * @param error
     * @param user
     * @param request
     * @return
     */
    @RequestMapping("getInQueue")
    public String getInQueue(@ModelAttribute(value = "queueElement") QueueElement qe, BindingResult error, @ModelAttribute(value = "userInfo") User user, HttpServletRequest request) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        User userSession = (User) request.getSession().getAttribute("user");
        user.setEmail(userSession.getEmail());
        user.setfName(userSession.getfName());
        user.setlName(userSession.getlName());
        user.setSubjects(RetrieveInfo.getSubjects(userSession.getEmail()));
        user.setUser_type(userSession.getUser_type());
        System.out.println("getInQueue fname: " + user.getfName());
        ArrayList<Integer> numExercises = RetrieveInfo.getUnapprovedExercises(userSession.getEmail(),
                (String) request.getSession().getAttribute("selectedValue"));
        request.getSession().setAttribute("exerciseList", numExercises);
        request.getSession().setAttribute("user", userSession);
        return "WEB-INF/jsp/queueUp.jsp";
        // </editor-fold>
    }

}
