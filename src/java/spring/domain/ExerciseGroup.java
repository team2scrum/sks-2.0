/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.domain;
import java.util.ArrayList;

/**
 *
 * @author Ruben
 */
public class ExerciseGroup {
	private int groupID;
	private ArrayList<Integer> exercises = new ArrayList<Integer>();
	private int mandatory;
	private String subjectCode;

	public int getGroupID() {
		return groupID;
	}

	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}

	public ArrayList<Integer> getExercises() {
		return exercises;
	}

	public void setExercises(ArrayList<Integer> exercises) {
		this.exercises = exercises;
	}
	

	public int getMandatory() {
		return mandatory;
	}

	public void setMandatory(int mandatory) {
		this.mandatory = mandatory;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}
	
	public void addExercise(Integer exercise){
		exercises.add(exercise);
	}
	
	@Override
	public String toString() {
		return subjectCode + ": " + exercises.toString() + ", " + mandatory;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (obj instanceof ExerciseGroup) {
			ExerciseGroup e = (ExerciseGroup) obj;
			return groupID == e.getGroupID() && subjectCode.equalsIgnoreCase(e.getSubjectCode());
		}
		return false;
	}
	
}
