package spring.domain;

import org.springframework.context.annotation.Scope;
import business.Encryption;
import org.hibernate.validator.constraints.Email;


@Scope("session")
public class User {

	
    String fName;
    String lName;
    String email;
    Integer user_type;
    String[] subjects;
	String password;
	boolean readyForExam;
	byte[] cryptPass;
    User user;

	public User(){}

	public User(String fName, String lName, String email, Integer user_type, String[] subjects, byte[] cryptPass) {
		this.fName = fName;
		this.lName = lName;
		this.email = email;
		this.user_type = user_type;
		this.subjects = subjects;
		this.cryptPass = cryptPass;
	}
	
		
	public void setCryptPass(byte[] cryptPass) {
		this.cryptPass = cryptPass;
	}

	public byte[] getCryptPass() {
		return cryptPass;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = "dummy";
		Encryption ec = new Encryption();
		cryptPass = ec.encrypt(password);
	}

	public void setSubjects(String[] subjects) {
		this.subjects = subjects;
	}

	public String[] getSubjects() {
		return subjects;
	}

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public Integer getUser_type() {
        return user_type;
    }
	
	public String getuserTypeTitle() {
		if (user_type == null) return "User type not defined";
		switch (user_type.intValue()) {
			case 0: return "Administrator";
			case 1: return "Faglærer";
			case 2: return "Studentassistent";
			case 3: return "Student";
			default: return "Unknown user type";
		}
	}

    public void setUser_type(Integer user_type) {
        this.user_type = user_type;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void setUser(User user){
        this.user = user;
    }

	public boolean isReadyForExam() {
		return readyForExam;
	}

	public void setReadyForExam(boolean readyForExam) {
		this.readyForExam = readyForExam;
	}
	

	@Override
	public String toString(){
		return fName +", "+ lName +", "+ email +", "+ user_type;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj instanceof User) {
			User u = (User) obj;
			return email.equals(u.getEmail());
		}
		return false;
	}
}
