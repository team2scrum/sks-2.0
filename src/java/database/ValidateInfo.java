package database;

import java.sql.*;

public class ValidateInfo extends DBConnection {
	
	/**
	 * Checks if an user exists in the database or not
	 * using the users email
	 * @param email
	 * @return 
	 */
	public static boolean userExist(String email) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		int e = -1;
		try {
			conn.setAutoCommit(false);
			
			String sql = "select count(email) from Users where email like ?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setString(1, email);
			
			res = prep.executeQuery();
			res.next();
			e = res.getInt(1);
			
			conn.commit();
			
		} catch (SQLException ex) {
			System.out.println("Error @ Control.userExists() : "+ ex.getMessage());
		}
		
		return e != 0;
		// </editor-fold>
	}
	
	/**
	 * Checks if a word exists in the most common 50k words
	 * @param word
	 * @return 
	 */
	public static boolean isSimpleWord(String word) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		int e = -1;
		try {
			conn.setAutoCommit(false);
			
			String sql = "select count(word) from Simple_words where word like ?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setString(1, word);
			
			res = prep.executeQuery();
			res.next();
			e = res.getInt(1);
			
			conn.commit();
			
		} catch (SQLException ex) {
			System.out.println("Error @ Control.userExists() : "+ ex.getMessage());
		}
		
		return e != 0;
		// </editor-fold>
	}
	
	
}
