package database;

import java.sql.*;

public class DBConnection {
	
	private static final String databaseName = "jdbc:derby://db.stud.aitel.hist.no:1527/13ing1gr11;user=team11;password=grQ24A";
	//private static final String databaseName = "jdbc:derby://localhost:1527/skstest;user=team11;password=team11";

	protected static Connection conn;
	protected static Statement stm;
	protected static ResultSet res;
	
	/**
	 * Establish a connection to the database.
	 * The connection, statement and resultSet objects are available as <code>conn, stm, res</code>.
	 * @return <code>true</code> if operation was successful, <code>false</code> otherwise.
	 */
	public static boolean makeConnection() {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		String databaseDriverJDB = "org.apache.derby.jdbc.ClientDriver";

		// create a connection...
		try {
			Class.forName(databaseDriverJDB);
			// ... to the database named:
			conn = DriverManager.getConnection(databaseName);
			// open all needed resources
			stm = conn.createStatement();
			// set a global transaction isolation level
			conn.setTransactionIsolation(java.sql.Connection.TRANSACTION_READ_COMMITTED);
			
		} catch (ClassNotFoundException e1 ) {
			System.out.println("Error in makeConnection(): " + e1.getMessage());
			return false;
		}catch (SQLException e2) {
			System.out.println("Error in makeConnection(): " + e2.getMessage());
			return false;
		}
		
		return true;
		// </editor-fold>
	}

	/**
	 * Safely closes all database connections.
	 * @return <code>true</code> if operation was successful, <code>false</code> otherwise.
	 */
	public static boolean closeResources() {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			if (res != null) {
				res.close();
			}
			if (stm != null) {
				stm.close();
			}
			if (conn != null) {
				conn.rollback();
				conn.setAutoCommit(true);
				conn.close();
			}
		} catch (SQLException e) {
			System.out.println("Error closing resources: " + e.getMessage());
			return false;
		}
		return true;
		// </editor-fold>
	}
	
	/**
	 * Returns true if the connection is initialized (makeConnection() jas been called). False otherwise
	 * @return 
	 */
	public static boolean isInitialized() {
		return conn != null;
	}
}
