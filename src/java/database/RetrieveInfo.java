package database;

import static database.DBConnection.conn;
import java.sql.*;
import java.util.ArrayList;
import spring.domain.ExerciseGroup;
import spring.domain.User;

public class RetrieveInfo extends DBConnection {

	/**
	 * Returns an arrayList of every student, as Strings
	 * @return
	 */
	public static ArrayList<String> getStudentListString() {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			res = stm.executeQuery("select first_name, last_name, email from Users where user_type = 3");
			conn.commit();

			ArrayList students = new ArrayList();
			while (res.next()) {
				String student = res.getString(1) + ", " + res.getString(2) + ", " + res.getString(3);
				students.add(student);
			}

			return students;

		} catch (SQLException e) {
			System.out.println("Error in getStudentList(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}

	/**
	 * Returns an arrayList of every user in the database, as User objects.
	 * @return
	 */
	public static ArrayList<User> getStudentListUser() {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			res = stm.executeQuery("select first_name, last_name, email from Users where user_type = 3");
			conn.commit();

			ArrayList students = new ArrayList();
			while (res.next()) {
				User student = new User();

				student.setfName(res.getString("first_name"));
				student.setlName(res.getString("last_name"));
				student.setEmail(res.getString("email"));
				Blob b = res.getBlob("password");
				student.setCryptPass(b.getBytes(1, (int) b.length()));
				student.setUser_type(2);

				students.add(student);
			}

			return students;

		} catch (SQLException e) {
			System.out.println("Error in getStudentList(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}

	/**
	 * Returns the user type of the selected user. user types are: 0:administrator 1:teacher 2:student assistant 3:student
	 *
	 * @param email
	 * @return
	 */
	public static int getUserType(String email) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "select USER_TYPE from TEAM11.USERS where EMAIL = ? ";
			PreparedStatement preparedGetAccess = conn.prepareStatement(sql);
			preparedGetAccess.setString(1, email);

			res = preparedGetAccess.executeQuery();

			conn.commit();
			res.next();

			return res.getInt(1); //returns a number between 0-3:
			//0 is admin
			//1 is teacher
			//2 is student assistant
			//3 is student

		} catch (SQLException e) {
			System.out.println("Error in getUserType(): " + e.getMessage());
			return -1;
		}
		// </editor-fold>
	}

	/**
	 * Returns a user object
	 *
	 * @param email
	 * @return
	 */
	public static User getUser(String email) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "select * from Users where email = ?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setString(1, email);
			res = prep.executeQuery();
			conn.commit();

			User student = new User();
			res.next();

			student.setfName(res.getString("first_name"));
			student.setlName(res.getString("last_name"));
			student.setEmail(res.getString("email"));
			Blob b = res.getBlob("password");
			student.setCryptPass(b.getBytes(1, (int) b.length()));
			student.setUser_type(res.getInt("user_type"));

			student.setSubjects(getSubjects(email));

			return student;

		} catch (SQLException e) {
			System.out.println("Error in getAccessFromMail(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}

	/**
	 * Returns a String array containing all the subjects a given student attends. Returns the subjects with subject code and subject name concatenated.
	 *
	 * @param studentMail
	 * @return
	 */
	public static String[] getSubjects(String studentMail) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "select subjects.subject_code, subject_name from USERS_SUBJECTS join subjects\n"
					+ " on USERS_SUBJECTS.SUBJECT_CODE = subjects.SUBJECT_CODE where USER_EMAIL = ? and isStudAssistant = 0";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setString(1, studentMail);

			res = prep.executeQuery();
			conn.commit();

			ArrayList<String> tmp = new ArrayList<String>();
			while (res.next()) {
				String subject = res.getString("subject_code") + " " + res.getString("subject_name");
				if (subject.length() > 56) {
					subject = subject.substring(0, 53) + "...";
				}
				tmp.add(subject);
			}

			String[] subs = new String[tmp.size()];
			for (int i = 0; i < tmp.size(); i++) {
				subs[i] = tmp.get(i);
			}

			return subs;
		} catch (SQLException e) {
			System.out.println("Error in getSubjects(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}

	/**
	 * Returns an array of subject codes from subjects the student attends
	 *
	 * @param studentMail
	 * @return
	 */
	public static String[] getSubjectCodes(String studentMail) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "select subjects.subject_code from USERS_SUBJECTS join subjects\n"
					+ " on USERS_SUBJECTS.SUBJECT_CODE = subjects.SUBJECT_CODE where USER_EMAIL = ? and isStudAssistant = 0";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setString(1, studentMail);

			res = prep.executeQuery();
			conn.commit();

			ArrayList<String> tmp = new ArrayList<String>();
			while (res.next()) {
				tmp.add(res.getString("subject_code"));
			}

			String[] subs = new String[tmp.size()];
			for (int i = 0; i < tmp.size(); i++) {
				subs[i] = tmp.get(i);
			}

			return subs;
		} catch (SQLException e) {
			System.out.println("Error in getSubjectCodess(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}

	/**
	 * Returns an array of the name for every registered subject.
	 *
	 * @return
	 */
	public static String[] getSubjectList() {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			res = stm.executeQuery("select SUBJECT_CODE, SUBJECT_NAME from SUBJECTS");
			conn.commit();

			ArrayList subjects = new ArrayList();
			while (res.next()) {
				String subject = res.getString(1) + "   " + res.getString(2);
				if (subject.length() > 56) {
					subject = subject.substring(0, 53) + "...";
				}
				subjects.add(subject);
			}

			String[] ret = new String[subjects.size()];
			subjects.toArray(ret);

			return ret;

		} catch (SQLException e) {
			System.out.println("Error in getSubjectList(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}

	/**
	 * Evaluates whether or not a given student has approved all the requirements in a given subject.
	 *
	 * @param studentMail
	 * @param subjectCode
	 * @return true or false.
	 */
	public static boolean isReadyForExam(String studentMail, String subjectCode) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		if (!hasSufficiantNumberOfExercises(studentMail, subjectCode)) {
			return false;
		}

		try {
			conn.setAutoCommit(false);
			String sql = "select sum(approved) as num_approved, REQUIREMENTS.NUM_MANDATORY from groups join requirements\n"
					+ " on groups.EX_GROUP = requirements.EX_GROUP and groups.SUBJECT_CODE = REQUIREMENTS.SUBJECT_CODE\n"
					+ " join EXERCISES on GROUPS.EXERCISE = EXERCISES.EXERCISE\n"
					+ " where groups.SUBJECT_CODE = ?\n"
					+ " and EXERCISES.USER_EMAIL = ?\n"
					+ " and EXERCISES.SUBJECT_CODE = ?\n"
					+ " group by requirements.EX_GROUP, REQUIREMENTS.NUM_MANDATORY";

			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setString(1, subjectCode);
			prep.setString(2, studentMail);
			prep.setString(3, subjectCode);

			res = prep.executeQuery();
			conn.commit();

			boolean ready = true;
			while (res.next() && ready) {
				ready &= res.getInt("num_approved") >= res.getInt("num_mandatory");
			}

			return ready;

		} catch (SQLException e) {
			System.out.println("Error in isReadyExam(): " + e.getMessage());
			return false;
		}

		// </editor-fold>
	}

	/**
	 * 
	 * @param studentMail
	 * @param subjectCode
	 * @return 
	 */
   private static boolean hasSufficiantNumberOfExercises(String studentMail, String subjectCode) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "select sum(approved) as amount from users join exercises on users.EMAIL = exercises.USER_EMAIL\n"
					+ " where EXERCISES.SUBJECT_CODE = ? and EXERCISES.USER_EMAIL = ?\n"
					+ " group by EXERCISES.USER_EMAIL having sum(approved) >= (\n"
					+ " select minimum_req from subjects where subject_code = ?)";
			PreparedStatement prep = conn.prepareStatement(sql);

			prep.setString(1, subjectCode);
			prep.setString(2, studentMail);
			prep.setString(3, subjectCode);
			res = prep.executeQuery();
			conn.commit();

			return res.next();

		} catch (SQLException e) {
			System.out.println("Error in hasSufficientNumberOfExercises(): " + e.getMessage());
			return false;
		}
		// </editor-fold>
	}

	/**
	 * Returns an ArrayList<Integer> containing all exercises in a given subject from given user.
	 *
	 * @param studentMail
	 * @param subjectCode
	 * @return 1: exercise approved, 0: not approved
	 */
	public static ArrayList<Integer> getUserExercises(String studentMail, String subjectCode) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);

			String sql = "select EXERCISES.APPROVED from USERS join "
					+ " EXERCISES on USERS.EMAIL = EXERCISES.USER_EMAIL"
					+ " where EXERCISES.SUBJECT_CODE = ? "
					+ " and USERS.EMAIL = ?";
			PreparedStatement prep = conn.prepareStatement(sql);

			prep.setString(1, subjectCode);
			prep.setString(2, studentMail);
			res = prep.executeQuery();
			conn.commit();

			ArrayList<Integer> list = new ArrayList<Integer>();
			while (res.next()) {
				list.add(res.getInt(1));
			}

			return list;

		} catch (SQLException e) {
			System.out.println("Error in exercisesDone(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}
	
	/**
	 * Returns an arrayList of every unaproved exercise in a subject for a given student
	 * @param studentMail
	 * @param subjectCode
	 * @return 
	 */
	public static ArrayList<Integer> getUnapprovedExercises(String studentMail, String subjectCode) {
		ArrayList<Integer> a = getUserExercises(studentMail, subjectCode);
		ArrayList<Integer> temp;
		temp = new ArrayList();
		int j = 0;
		for (int i = 0; i < a.size(); i++) {
			if(a.get(i) == 0){
				temp.add(i+1);
			}
		}
		return temp;
	}

	/**
	 * Get the number of exercises in a specific subject
	 * @param subjectCode
	 * @return 
	 */

	public static int getNumberOfExercises(String subjectCode){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
	try {
		conn.setAutoCommit(false);
		String sql = "select NUMBER_OF_EXERCISES from TEAM11.SUBJECTS where  SUBJECT_CODE= ? ";
		PreparedStatement preparedGetAccess = conn.prepareStatement(sql);
		preparedGetAccess.setString(1, subjectCode);

		res = preparedGetAccess.executeQuery();

		conn.commit();
		res.next();

		return res.getInt(1); 

	} catch (SQLException e) {
		System.out.println("Error in getNumberOfExercises(): " + e.getMessage());
		return -1;
	}
	// </editor-fold>
	}

	private static int getMandatory(String subjectCode,int groupID){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
	try {
		conn.setAutoCommit(false);
		String sql = "select num_mandatory from requirements where"
				+ " requirements.SUBJECT_CODE = ? and requirements.EX_GROUP = ?";
		PreparedStatement preparedGetAccess = conn.prepareStatement(sql);
		preparedGetAccess.setString(1, subjectCode);
		preparedGetAccess.setInt(2, groupID);

		res = preparedGetAccess.executeQuery();

		conn.commit();
		res.next();

		return res.getInt(1); 

	} catch (SQLException e) {
		System.out.println("Error in getMandatory(): " + e.getMessage());
		return -1;
	}
	// </editor-fold>
	}

	/**
	 * Returns a Integer array with all the groupIDs in a specific subject.
	 * @param subjectCode
	 * @return 
	 */
	public static ArrayList<Integer> getGroupIDs (String subjectCode){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "select ex_group from groups where subject_code = ?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setString(1, subjectCode);
			res = prep.executeQuery();
			conn.commit();

			ArrayList<Integer> temp = new ArrayList();
			int inc = 0;
			while (res.next()) {
				int i = (res.getInt(1));
				if (temp.isEmpty()) {
					temp.add(i);
				} else {
					if (i > temp.get(inc)) {
						temp.add(i);
						inc++;
					}
				}

			}
			return temp;

		} catch (SQLException e) {
			System.out.println("Error in getGroupIDs(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}
	
	/**
	 * Returns an arrayList of ExerciseGroup objects, derived from a given subject
	 * @param subjectCode
	 * @return 
	 */
	public static ArrayList<ExerciseGroup> getExerciseGroups (String subjectCode){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			ArrayList<Integer> IDs = getGroupIDs(subjectCode);
			ArrayList<ExerciseGroup> temp;
			temp = new ArrayList();
			for (int i = 0; i < IDs.size(); i++) {

				ExerciseGroup a = new ExerciseGroup();
				a.setGroupID(IDs.get(i));
				a.setExercises(getExercisesFromGroup(subjectCode, IDs.get(i)));
				a.setMandatory(getMandatory(subjectCode, IDs.get(i)));
				a.setSubjectCode(subjectCode);
				temp.add(a);
			}

			return temp;

		} catch (SQLException e) {
			System.out.println("Error in getExerciseGroups(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}
	
	/**
	 * Returns all exercises in a given subject and group id
	 * @param subjectCode
	 * @param groupID
	 * @return 
	 */
	public static ArrayList<Integer> getExercisesFromGroup (String subjectCode, int groupID){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "select exercise from groups where subject_code = ? and ex_group = ?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setString(1, subjectCode);
			prep.setInt(2, groupID);
			res = prep.executeQuery();
			conn.commit();

			ArrayList<Integer> temp = new ArrayList();
			int inc = 0;
			while (res.next()) {
				int i = (res.getInt(1));
				temp.add(i);
			}
			return temp;

		} catch (SQLException e) {
			System.out.println("Error in getExercisesFromGroup(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}
	
	/**
	 * Check if a user has a subject
	 *
	 * @param studentMail
	 * @param subjectCode
	 * @return
	 */
	public static boolean existsInSubject(String studentMail, String subjectCode) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);

			String sql = "select * from users_subjects "
					+ "where user_email = ? "
					+ "and subject_code = ? "
					+ "and isStudAssistant = 0";
			PreparedStatement prep = conn.prepareStatement(sql);

			prep.setString(1, studentMail);
			prep.setString(2, subjectCode);
			res = prep.executeQuery();
			conn.commit();

			return res.next();

		} catch (SQLException e) {
			System.out.println("Error in exercisesDone(): " + e.getMessage());
			return false;
		}
		// </editor-fold>
	}

	/**
	 * Returns a String array with every student in a given subject
	 * @param subjectCode
	 * @return
	 */
	public static String[] getStudentsFromSubject(String subjectCode) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "select first_name, last_name, email from Users join users_subjects on users.EMAIL = users_subjects.USER_EMAIL join \n"
					+ "subjects on users_subjects.SUBJECT_CODE = subjects.SUBJECT_CODE \n"
					+ "where subjects.subject_code = ? and users.USER_TYPE = 3";

			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setString(1, subjectCode);
			res = prep.executeQuery();
			conn.commit();

			ArrayList<User> students = new ArrayList();
			while (res.next()) {
				User student = new User();

				student.setfName(res.getString("first_name"));
				student.setlName(res.getString("last_name"));
				student.setEmail(res.getString("email"));

				students.add(student);
			}
			for (int i = 0; i < students.size(); i++) {
				students.get(i).setReadyForExam(isReadyForExam(students.get(i).getEmail(), subjectCode));
			}

			String[] temp = new String[students.size()];
			for (int i = 0; i < temp.length; i++) {
				temp[i] = students.get(i).getEmail() + ", " + students.get(i).getlName() + ", "
						+ students.get(i).getfName() + ", ";
				if (students.get(i).isReadyForExam()) {
					temp[i] += "Godkjent";
				} else {
					temp[i] += "Ikke godkjent";
				}
			}
			return temp;

		} catch (SQLException e) {
			System.out.println("Error in getStudentsFromSubject(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}

	/**
	 * Returns encrypted password from given user email
	 *
	 * @param email
	 * @return
	 */
	public static byte[] getPassword(String email) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "select password from users where email = ?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setString(1, email);
			res = prep.executeQuery();
			conn.commit();

			res.next();
			Blob b = res.getBlob("password");
			byte[] pass = b.getBytes(1, (int) b.length());

			return pass;

		} catch (SQLException e) {
			System.out.println("Error in getAccessFromMail(): " + e.getMessage());
			return null;
		}
		// </editor-fold>
	}

}
