package database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import spring.domain.ExerciseGroup;
import spring.domain.User;

public class AddInfo extends DBConnection {

    /**
     * Registers a list of users
     * @param userList
     * @return
     */
    public static boolean registerUser(List<User> userList) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        try {
			conn.setAutoCommit(false);
            String sql = "insert into Users values (?,?,?,?,?)";
            PreparedStatement prepStm = conn.prepareStatement(sql);
            Blob blob = conn.createBlob();
            for (User aUser : userList) {
                blob.setBytes(1, aUser.getCryptPass());
                prepStm.setString(1, aUser.getfName());
                prepStm.setString(2, aUser.getlName());
                prepStm.setString(3, aUser.getEmail());
                prepStm.setBlob(4, blob);
                prepStm.setInt(5, aUser.getUser_type());
                prepStm.addBatch();
            }
            
            prepStm.executeBatch();
            conn.commit();
            return true;

        } catch (SQLException e) {
            System.out.println("Error @ AddInfo.registerUser(): " + e.getMessage());
            return false;
        }
        // </editor-fold>
    }
	
	/**
     * Register a single user
     * @param user
     * @return
     */
    public static boolean registerUser(User user) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        try {
			conn.setAutoCommit(false);
            String sql = "insert into Users values (?,?,?,?,?)";
            PreparedStatement prepStm = conn.prepareStatement(sql);
            Blob blob = conn.createBlob();
			
			blob.setBytes(1, user.getCryptPass());
			prepStm.setString(1, user.getfName());
			prepStm.setString(2, user.getlName());
			prepStm.setString(3, user.getEmail());
			prepStm.setBlob(4, blob);
			prepStm.setInt(5, user.getUser_type());
			prepStm.addBatch();
            
            prepStm.executeUpdate();
            conn.commit();
            return true;

        } catch (SQLException e) {
            System.out.println("Error @ AddInfo.registerUser(): " + e.getMessage());
            return false;
        }
        // </editor-fold>
    }
	
	/**
	 * Registers a new user, 
	 * registers the user to subjects, 
	 * and adds necessary exercises
	 * @param userList
	 * @return 
	 */
	public static boolean registerUserWithSubjects(List<User> userList){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        try {
			conn.setAutoCommit(false);
            String sqlUsr = "insert into Users values (?,?,?,?,?)";
            PreparedStatement prepStmUser = conn.prepareStatement(sqlUsr);
			String sqlSub = "insert into Users_subjects values(?,?,default)";
			PreparedStatement prepStmSub = conn.prepareStatement(sqlSub);
			
			Blob blob = conn.createBlob();
			//adds user
            for (User aUser : userList) {
                blob.setBytes(1, aUser.getCryptPass());
                prepStmUser.setString(1, aUser.getfName());
                prepStmUser.setString(2, aUser.getlName());
                prepStmUser.setString(3, aUser.getEmail());
                prepStmUser.setBlob(4, blob);
                prepStmUser.setInt(5, aUser.getUser_type());
                prepStmUser.executeUpdate();
				
				String mail = aUser.getEmail();
				int usertype = aUser.getUser_type();
				//adds subjects
				prepStmSub.setString(1, mail);
				for (String sub : aUser.getSubjects()){
					prepStmSub.setString(2, sub);
					prepStmSub.executeUpdate();
					
					//adds exercises
					registerExercises(sub, mail, usertype);
				}
            }
			
            conn.commit();
            return true;

        } catch (SQLException e) {
            System.out.println("Error @ AddInfo.registerUserWithSubjects(): " + e.getMessage());
            return false;
        }
        // </editor-fold>
	}
	
	/**
	 * Registers an assistant to a subject
	 * @param mail
	 * @param subject_code
	 * @return 
	 */
	public static boolean registerAssistantToSubject(String mail, String subject_code){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        try {
			conn.setAutoCommit(false);
			String sqlSub = "insert into Users_subjects values(?,?,1)";
			PreparedStatement prepStmSub = conn.prepareStatement(sqlSub);
			int usertype = 2;
			
			prepStmSub.setString(1, mail);
			prepStmSub.setString(2, subject_code);
			prepStmSub.executeUpdate();
			
			registerExercises(subject_code, mail, usertype);
			
            conn.commit();
            return true;

        } catch (SQLException e) {
            System.out.println("Error @ AddInfo.registerAssistantToSubject(): " + e.getMessage());
            return false;
        }
        // </editor-fold>
	}
	
	/**
	 * Registers a user to a subject, 
	 * and adds necessary exercises
	 * @param u
	 * @return 
	 */
	public static boolean registerUserToSubjects(User u){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        try {
			conn.setAutoCommit(false);
			String sqlSub = "insert into Users_subjects values(?,?, default)";
			PreparedStatement prepStmSub = conn.prepareStatement(sqlSub);
			String mail = u.getEmail();
			int usertype = u.getUser_type();
			
			for (String sub : u.getSubjects()){
				prepStmSub.setString(1, mail);
				prepStmSub.setString(2, sub);
				prepStmSub.addBatch();
				
				registerExercises(sub, mail, usertype);
			}
			prepStmSub.executeBatch();
            conn.commit();
            return true;

        } catch (SQLException e) {
            System.out.println("Error @ AddInfo.registerUserToSubjects(): " + e.getMessage());
            return false;
        }
        // </editor-fold>
	}
	
	/**
	 * Registers a user to a subject, 
	 * and adds necessary exercises
	 * @param mail
	 * @param subject_code
	 * @return 
	 */
	public static boolean registerUserToSubject(String mail, String subject_code){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        try {
			conn.setAutoCommit(false);
			String sqlSub = "insert into Users_subjects values(?,?, default)";
			PreparedStatement prepStmSub = conn.prepareStatement(sqlSub);
			int usertype = 3;
			
			prepStmSub.setString(1, mail);
			prepStmSub.setString(2, subject_code);
			prepStmSub.executeUpdate();
			
			registerExercises(subject_code, mail, usertype);
			
            conn.commit();
            return true;

        } catch (SQLException e) {
            System.out.println("Error @ AddInfo.registerUserToSubjects(): " + e.getMessage());
            return false;
        }
        // </editor-fold>
	}
	
	private static void registerExercises(String sub, String mail, int usertype) throws SQLException {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">		
		if (usertype != 3){
			System.out.println("ERROR @ registering exercises : user must be of type student");
		}
		//adds exercises
		String sqlEx = "insert into Exercises values (?,?,?,0)"; //Exercises are by not approved by default
		PreparedStatement prepStmEx = conn.prepareStatement(sqlEx);
		
		String sqlcount = "select number_of_exercises from subjects where subject_code = ?";
		PreparedStatement prepcount = conn.prepareStatement(sqlcount);
		prepcount.setString(1, sub);
		res = prepcount.executeQuery();
		res.next();
		int t = res.getInt("number_of_exercises");
		for (int i = 1; i <= t; i++) {
			prepStmEx.setString(1, mail);
			prepStmEx.setString(2, sub);
			prepStmEx.setInt(3, i);

			prepStmEx.executeUpdate();
		}
		// </editor-fold>
	}
	
	/**
     * Registers a new subject in the database. 
	 * Professor referees to his email
	 * PS: inputs should be changed to a bean object when possible
     *
     * @param subject_code
     * @param subject_name
     * @param min_req
	 * @param number_of_ex
	 * @param professor
     * @return
     */
	public static boolean addSubject(String subject_code, String subject_name, int min_req, int number_of_ex, String professor) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "insert into SUBJECTS values (?,?,?,?,?)";
			PreparedStatement prepStm = conn.prepareStatement(sql);
			
			int type = RetrieveInfo.getUserType(professor);
			if(type <=1){
				prepStm.setString(1, subject_code);
				prepStm.setString(2, subject_name);
				prepStm.setInt(3, min_req);
				prepStm.setInt(4, number_of_ex);
				prepStm.setString(5, professor);

				prepStm.executeUpdate();

				conn.commit();
				return true;
			}
			else{
				System.out.println("Error at AddInfo.addSubject(): The current user type can not be flagged as professor");
				return false;
			}
			
		} catch (SQLException e) {
			System.out.println("Error @ AddInfo.addSubject(): " + e.getMessage());
			return false;
		}
		// </editor-fold>
	}

    /**
     * Registers a requirement in the database. 
	 * The primary key is a combination of subject and groupID 
	 * PS: inputs should be changed to a bean object when possible
	 * groupID could be generated automatically
     *
     * @param subject
     * @param groupID
     * @param num_mandatory
     * @return
     */
    public static boolean addRequirements(String subject, int groupID, int num_mandatory) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        try {
			conn.setAutoCommit(false);
            String sql = "insert into Requirements values (?,?,?)";
            PreparedStatement prepStm = conn.prepareStatement(sql);

            prepStm.setString(1, subject);
            prepStm.setInt(2, groupID);
            prepStm.setInt(3, num_mandatory);

            prepStm.executeUpdate();
            conn.commit();
            return true;

        } catch (SQLException e) {
            System.out.println("Error @ AddInfo.addRequirements(): " + e.getMessage());
            return false;
        }
        // </editor-fold>
    }

    /**
     * Registers a exercise into the group table in the database.
	 * The primary key is a combination of subject and exercise 
	 * PS: inputs should be changed to a bean object when possible 
	 * the group ID in the specific subject must exist in the
     * requirement table.
     *
     * @param subject
     * @param exercise_number
     * @param groupID
     * @return
     */
    public static boolean addExerciseToGroup(String subject, int groupID, int exercise_number) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        try {
			conn.setAutoCommit(false);
            String sql = "insert into GROUPS values (?,?,?)";
            PreparedStatement prepStm = conn.prepareStatement(sql);

            prepStm.setString(1, subject);
            prepStm.setInt(2, exercise_number);
            prepStm.setInt(3, groupID);

            prepStm.executeUpdate();
            conn.commit();
            return true;

        } catch (SQLException e) {
            System.out.println("Error @ AddInfo.addExerciseToGroup(): " + e.getMessage());
            return false;
        }
        // </editor-fold>
    }
	
	/**
	 * Adds groups and requirements to a subject
	 * @param subject
	 * @param exGroup
	 * @return 
	 */
	  public static boolean addFullReq(String subject, ArrayList<ExerciseGroup> exGroup) {
        // <editor-fold defaultstate="collapsed" desc="Collapsed Code">
        try {
			conn.setAutoCommit(false);
            for (int i = 0; i < exGroup.size(); i++) {
					addRequirements(subject, exGroup.get(i).getGroupID(), exGroup.get(i).getMandatory());
					addExercisesToGroup(subject, exGroup.get(i).getGroupID(), exGroup.get(i).getExercises());
					}
			
			return true;

        } catch (SQLException e) {
            System.out.println("Error @ AddInfo.addFullReq(): " + e.getMessage());
            return false;
        }
        // </editor-fold>
    }


	
	 /**
     * Registers a array of exercises into the group table in the database.
	 * The primary key is a combination of subject and exercise 
	 * PS: inputs should be changed to a bean object when possible 
	 * the group ID in the specific subject must exist in the
     * requirement table.
	 * None of the exercises can already exist in the selected group!
     *
     * @param subject
     * @param exercise_number
     * @param groupID
     * @return
     */
	public static boolean addExercisesToGroup(String subject, int groupID, ArrayList<Integer> exercise_number ) {
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String sql = "insert into GROUPS values (?,?,?)";
			PreparedStatement prepStm = conn.prepareStatement(sql);
			
			for (int i = 0; i < exercise_number.size(); i++) {
			
			prepStm.setString(1, subject);
			prepStm.setInt(2, exercise_number.get(i));
			prepStm.setInt(3, groupID);
			
			prepStm.executeUpdate();
			}
			conn.commit();
			return true;
			
		} catch (SQLException e) {
			System.out.println("Error @ AddInfo.addExercisesToGroup(): " + e.getMessage());
			return false;
		}
		// </editor-fold>
	}
		

}
