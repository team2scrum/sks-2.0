package database;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import spring.domain.User;


public class EditInfo extends DBConnection{
	
	/**
	 * Registers an exercise as approved in the database
	 * @param email
	 * @param subject
	 * @param exercise
	 * @return 
	 */
	public static boolean approveExercise(String email, String subject, int exercise){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String flagString = "update EXERCISES set APPROVED=1 where USER_EMAIL = ? and SUBJECT_CODE = ? and EXERCISE = ?";
			PreparedStatement preparedFlagExercise = conn.prepareStatement(flagString);
			preparedFlagExercise.setString(1, email);
			preparedFlagExercise.setString(2, subject);
			preparedFlagExercise.setInt(3, exercise);

			preparedFlagExercise.executeUpdate();
			conn.commit();

			return true;
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException er) {
				System.out.println("Rollback error");
			}
			System.out.println("ERROR in EditInfo.ApproveExercise "+ e.getMessage() );
			return false;
		}
		// </editor-fold>
	}
        
        /**
	 * Registers an exercise as approved in the database
	 * @param email
	 * @param subject
	 * @param exercise
	 * @return 
	 */
	public static boolean approveExercise(String email, String subject, int[] exercises){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String flagString = "update EXERCISES set APPROVED=1 where USER_EMAIL = ? and SUBJECT_CODE = ? and EXERCISE = ?";
			PreparedStatement preparedFlagExercise = conn.prepareStatement(flagString);
                        for(int i = 0;i<exercises.length;i++){
			preparedFlagExercise.setString(1, email);
			preparedFlagExercise.setString(2, subject);
			preparedFlagExercise.setInt(3, exercises[i]);

			preparedFlagExercise.executeUpdate();
                        }
			conn.commit();

			return true;
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException er) {
				System.out.println("Rollback error");
			}
			System.out.println("ERROR in EditInfo.ApproveExercise "+ e.getMessage() );
			return false;
		}
		// </editor-fold>
	}
	
	/**
	 * Remove approvement from an exercise
	 * @param email
	 * @param subject
	 * @param exercise
	 * @return 
	 */
	public static boolean cancelApprovement(String email, String subject, int exercise){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String flagString = "update EXERCISES set APPROVED=0 where USER_EMAIL = ? and SUBJECT_CODE = ? and EXERCISE = ?";
			PreparedStatement preparedFlagExercise = conn.prepareStatement(flagString);
			preparedFlagExercise.setString(1, email);
			preparedFlagExercise.setString(2, subject);
			preparedFlagExercise.setInt(3, exercise);

			preparedFlagExercise.executeUpdate();
			conn.commit();

			return true;
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException er) {
				System.out.println("Rollback error");
			}
			System.out.println("ERROR in EditInfo.ApproveExercise "+ e.getMessage() );
		}
		return false;
		// </editor-fold>
	}
	
	/**
	 * Changes user password in the database
	 * @param user
	 * @param password
	 * @return 
	 */
	public static boolean changePassword(User user, byte[] password){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try{
			conn.setAutoCommit(false);
			String sql = "update USERS set password = ? where email = ?";
			PreparedStatement prep = conn.prepareStatement(sql);
			
			Blob blob = conn.createBlob();
			blob.setBytes(1, password);
			prep.setBlob(1, blob);
			prep.setString(2, user.getEmail());
			prep.executeUpdate();
			
			conn.commit();
			return true;
			
		} catch (SQLException e) {
			System.out.println("Error @ EditInfo.changePassword: "+ e.getMessage());
			return false;
		}
		// </editor-fold>
	}
	
	/**
	 * Changes user password in the database
	 * @param email 
	 * @param password
	 * @return 
	 */
	public static boolean changePassword(String email, byte[] password){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try{
			conn.setAutoCommit(false);
			String sql = "update USERS set password = ? where email = ?";
			PreparedStatement prep = conn.prepareStatement(sql);
			
			prep.setString(2, email);
			Blob blob = conn.createBlob();
			blob.setBytes(1, password);
			prep.setBlob(1, blob);
			prep.executeUpdate();
			
			conn.commit();
			return true;
			
		} catch (SQLException e) {
			System.out.println("Error @ EditInfo.changePassword: "+ e.getMessage());
			return false;
		}
		// </editor-fold>
	}
	
	/**
	 * Changes the number of exercises and the number of required exercises assigned to a specific subject
	 * @param newreq
	 * @param number_of_exercises
	 * @param subject_code
	 * @return 
	 */
	public static boolean changeExerciseInfo(int newreq, int number_of_exercises, String subject_code){
		// <editor-fold defaultstate="collapsed" desc="Collapsed Code">
		try {
			conn.setAutoCommit(false);
			String reqString = "update SUBJECTS set MINIMUM_REQ=?,  NUMBER_OF_EXERCISES=? where SUBJECT_CODE = ?";
			PreparedStatement preparedEdit = conn.prepareStatement(reqString);
			preparedEdit.setInt(1, newreq);
			preparedEdit.setInt(2, number_of_exercises);
			preparedEdit.setString(3, subject_code);
			

			preparedEdit.executeUpdate();
			conn.commit();

			return true;
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException er) {
				System.out.println("Rollback error");
			}
			System.out.println("ERROR in EditInfo.changeExerciseInfo "+ e.getMessage() );
		}
		return false;
		// </editor-fold>
	}	
	
}
