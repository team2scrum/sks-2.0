package business;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileCreator {
	
	public static String fileName;
	
	public static boolean createFile(String[] text, String filename){
		try{
			fileName = filename;
			File file = new File(fileName);
			FileWriter fw = new FileWriter(file, false);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for (String string : text) {
				bw.write(string);
				bw.newLine();
			}
			
			bw.close();
			fw.close();
			return true;
			
		}catch (IOException ioe){
			System.out.println("ERROR in createFile() : "+ ioe.getMessage());
			return false;
		}
	}
	
	public static void deleteFile(){
		File file = new File(fileName);
		file.delete();
	}
}
