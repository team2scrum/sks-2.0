package business;

import java.util.LinkedList;

public class Queue {

    private String subjectCode;
    private LinkedList<QueueElement> list;
    private int queueState = 0;

    /**
     * Default queue state is 1, Default subjectCode is N/A.
     */
    public Queue() {
        this(1);
        this.subjectCode = "Subject Code N/A";
    }

    /**
     * Sets queue state to required state, using default subject code.
     *
     * @param queueState
     */
    public Queue(int queueState) {
        if (queueState < 0 || queueState > 2) {
            return;
        }
        list = new LinkedList<QueueElement>();
        this.queueState = queueState;

    }

	/**
	 * Creates queue object with given queue state and subject code
	 * @param queueState
	 * @param subjectCode 
	 */
    public Queue(int queueState, String subjectCode) {
        if (queueState < 0 || queueState > 2) {
            return;
        }
        list = new LinkedList<QueueElement>();
        this.queueState = queueState;
        this.subjectCode = subjectCode;
    }

    /**
     * Return queue state: 0 : inactive queue, joining queue is restricted and
     * queues content is dropped 1 : active queue 2 : paused queue, joining
     * queue is restricted, but content stays
     *
     * @return
     */
    public int getQueueState() {
        return queueState;
    }

    /**
     * Sets queue state: 0 : inactive queue, joining queue is restricted and
     * queues content is dropped 1 : active queue 2 : paused queue, joining
     * queue is restricted, but content stays
     *
     * @param queueState
     */
    public void setQueueState(int queueState) {
        if (queueState < 0 || queueState > 2) {
            return;
        }
        this.queueState = queueState;
        if (queueState == 0) {
            clearQueue();
        }
    }

	/**
	 * Returns the subject code of queue.
	 * @return 
	 */
    public String getSubjectCode() {
        return subjectCode;
    }

	/**
	 * Sets a new subject code to the queue
	 * @param subjectCode 
	 */
    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

	/**
	 * Get QueueElement of given index
	 * @param index
	 * @return 
	 */
    public QueueElement getIndexElement(int index) {
        return list.get(index);
    }

	/**
	 * Adds a new QueueElement in queue.
	 * @param e
	 * @return 
	 */
    public boolean add(QueueElement e) {
        if (queueState == 1) {
            list.add(e);
            return true;
        }
        return false;
    }

	/**
	 * Returns the first element in queue
	 * @return 
	 */
    public QueueElement getFirst() {
        return list.getFirst();
    }

	/**
	 * Polls the first element in queue
	 * @return 
	 */
    public QueueElement poll() {
        return list.poll();
    }

	/**
	 * Removes an element from queue
	 * @param e 
	 */
    public void remove(QueueElement e) {
        list.remove(e);
    }

	/**
	 * Postpones an element by given amount of places
	 * @param e
	 * @param place 
	 */
    public void postpone(QueueElement e, int place) {
        if (place < 1) {
            return;
        }
        if (place >= list.size()) {
            list.poll();
            list.addLast(e);
        } else {
            QueueElement tmp = list.set(place, e);
            for (int i = place - 1; i > 0; i--) {
                tmp = list.set(i, tmp);
            }
            list.set(0, tmp);
        }
    }

	/**
	 * Postpones an element by given amount of places
	 * @param email
	 * @param place 
	 */
    public void postpone(String email, int place) {
        postpone(getElement(email), place);
    }

    /**
     * Returns a QueueElement from the queue, checking equality on email.
     *
     * @param email
     * @return A QueueElement from this queue, or null of no element exists with
     * this email.
     */
    public QueueElement getElement(String email) {
        for (QueueElement e : list) {
            if (e.getUser().getEmail().equalsIgnoreCase(email)) {
                return e;
            }
        }
        return null;
    }

    /**
     * Returns an array of every QueueElement in Queue
     *
     * @return
     */
    public QueueElement[] getElements() {
        QueueElement[] array = new QueueElement[list.size()];
        return list.toArray(array);
    }

	/**
	 * Clears queue (removes all elements).
	 */
    public void clearQueue() {
        list.clear();
    }

}
