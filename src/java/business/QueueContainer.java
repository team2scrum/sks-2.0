package business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

@SuppressWarnings("CallToThreadRun")
public class QueueContainer {

    private static HashMap<String, Queue> container = new HashMap();
    private static ArrayList<String> subjectNames = new ArrayList<String>(); 
    
	static Thread startTimer = new Thread() {
        @Override
		public void run() {
            Date start = new Date();
            long startTime = start.getTime();
            try {
                for(int i = 0;i<container.size();i++){
                    
                if (container.get(subjectNames.get(i)).getQueueState() != 0) {
                    for (int j= 0; j < container.get(subjectNames.get(i)).getElements().length; j++) {
                        container.get(subjectNames.get(i)).getIndexElement(j).incrementTimeInQueue();
                    }

                }
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                Date end = new Date();
                long endTime = end.getTime();
                try {
                    Thread.sleep((60000 - (endTime - startTime)));
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
                run();
            }
        }
    };
    
	/**
	 * Starts the queue timer in all queues.
	 */
	@SuppressWarnings("CallToThreadStartDuringObjectConstruction")
    public QueueContainer(){
        startTimer.start();
    }

	/**
	 * Adds a new queue to the container.
	 * @param subjectCode
	 * @param queue
	 * @return 
	 */
    public static boolean addQueue(String subjectCode, Queue queue) {
     if (container.containsKey(subjectCode)) {
            return false;
        }
        subjectNames.add(subjectCode);
        return container.put(subjectCode, queue) != null;
    }

	/**
	 * Gets a queue from given subject code.
	 * @param subjectCode
	 * @return 
	 */
    public static Queue getQueue(String subjectCode) {
        return container.get(subjectCode);
    }

}
