package business;

import java.util.*;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;

public class Encryption {
    
    final String key = "jKedOp28h67dEIRm817sed43";

    public byte[] encrypt(String str) {
        try {
            byte[] keyBytes = key.getBytes("ASCII");
            DESedeKeySpec keySpec = new DESedeKeySpec(keyBytes);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
            SecretKey secretKey;
            secretKey = factory.generateSecret(keySpec);
            
            byte[] text = str.getBytes("ASCII");
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return cipher.doFinal(text);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace(System.err);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace(System.err);
        } catch (InvalidKeyException e) {
            e.printStackTrace(System.err);
        } catch (IllegalBlockSizeException e){
            e.printStackTrace(System.err);
        } catch (BadPaddingException e){
            e.printStackTrace(System.err);
        } catch (UnsupportedEncodingException e){
            e.printStackTrace(System.err);
        } catch (InvalidKeySpecException e){
            e.printStackTrace(System.err);
        }
        return null;
    }
    
    public String decrypt(byte[] byt){
        try{
            byte[] keyBytes = key.getBytes("ASCII");
            DESedeKeySpec keySpec = new DESedeKeySpec(keyBytes);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
            SecretKey secretKey = factory.generateSecret(keySpec);
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return cipher.doFinal(byt).toString();
        }catch (NoSuchAlgorithmException e) {
            e.printStackTrace(System.err);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace(System.err);
        } catch (InvalidKeyException e) {
            e.printStackTrace(System.err);
        } catch (IllegalBlockSizeException e){
            e.printStackTrace(System.err);
        } catch (BadPaddingException e){
            e.printStackTrace(System.err);
        } catch (InvalidKeySpecException e){
            e.printStackTrace(System.err);
        } catch (UnsupportedEncodingException e){
           e.printStackTrace(System.err);
        }
        return null;
    }
    
	public boolean checkEqual(byte[] b1, byte[] b2){
		return Arrays.equals(b2, b2);
	}
	
    public String toString(byte[] array){
        String res = "";
        for(int i =0; i<array.length; i++){
            res+=array[i];
        }
        return res;
    }
}