package business;

import database.ValidateInfo;
import database.RetrieveInfo;
import database.AddInfo;
import spring.domain.User;
import org.apache.commons.lang3.RandomStringUtils;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class UserCreator {

	public String[] temp;
	public String[] users;
	private final Encryption enc = new Encryption();
	private String text;
	private String subject;
	private final int ADMIN = 0;
	private final int TEACHER = 1;
	private final int STUD_ASS = 2;
	private final int STUDENT = 3;

	
	/**
	 * The method creates several users from the user-list "users" and registers them to a subject in the database.
	 * If a user does not exists, it will create the user in the database, randomly generated a password that is sent to the user that were created and sent via e-mail.
	 * If a user already exists, the method will ad them to the selected subject in the database.
	 * Returns a String indicating weather the user was created and added to a subject, just added to the subject or not created or added.
	 * @return
	 */
	public String createUsers() {
		String res = "";
		users = text.split("\\r?\\n");
		List<User> userList = new ArrayList<User>();
		for (int i = 0; i < users.length; i++) {
			temp = users[i].split(",");
			if (temp.length == 3) {
				if (!temp[0].equals("\n") && temp[2].contains("@") && !ValidateInfo.userExist(temp[2])) {
					User user = new User();
					user.setlName(temp[0]);
					user.setfName(temp[1]);
					user.setEmail(temp[2]);
					user.setUser_type(STUDENT);
					String pw = RandomStringUtils.randomAlphanumeric(8);
					sendEmail(pw, temp[2]);
					byte[] pass = enc.encrypt(pw);
					user.setCryptPass(pass);
					res += "User " + user.getEmail() + " was created and added to the subject +"
							+ subject + "\n";
					userList.add(user);
				} else {
					User user = RetrieveInfo.getUser(temp[2]);
					if (user.getUser_type() == 3) {
						String[] subjectTemp = subject.split(" ");
						AddInfo.registerUserToSubject(user.getEmail(), subjectTemp[0]);
						res += "USer " + user.getEmail() + " was added to the subject " + subject;
					} else {
						res += "User was not added: " + temp[2];
					}
				}
			}
		}

		AddInfo.registerUser(userList);
		for(User aUser : userList){
			String[] subjectTemp = subject.split(" ");
			AddInfo.registerUserToSubject(aUser.getEmail(), subjectTemp[0]);
		}
		return res;
	}
	/**
	 * 
	 * @param user
	 * @param subject
	 * The method creates a single user. You can also select what kind of user to add (Administrator, Teacher, Student or Student-assistant).
	 * If the user does not exists it will be created and added to the database. A randomly generated password will also be sent via mail to the e-mail contained in the user-object.
	 * If the user already exists, the user will be added to the subject provided in the database.
	 * Returns a String indicating if the user was created and added to the subject, just added to the subject or not created or added to anything.
	 * 
	 * @return 
	 */
	public String createUser(User user, String subject) {
		String[] subjectTemp = subject.split(" ");
		String res = "";
		if (user.getEmail().contains("@") && !ValidateInfo.userExist(user.getEmail())) {
			String pw = RandomStringUtils.randomAlphanumeric(8);
			sendEmail(pw, user.getEmail());
			byte[] pass = enc.encrypt(pw);
			user.setCryptPass(pass);
			AddInfo.registerUser(user);
			AddInfo.registerUserToSubject(user.getEmail(), subjectTemp[0]);
			res+="User "+user.getEmail()+" was created and added to subject "+subject;
		} else {
			AddInfo.registerUserToSubject(user.getEmail(), subjectTemp[0]);
			res+="User "+user.getEmail()+" already exists. The user was added to subject "+subject;
		}
		return res;
	}

	private void sendEmail(String password, String email) {
		String to = email;
		String from = "team2scrum@gmail.com";
		if (to.contains("@")) {
			Properties properties = System.getProperties();
			properties.setProperty("mail.transport.protocol", "smtp");
			properties.setProperty("mail.host", "smtp.gmail.com");
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.port", "465");
			properties.put("mail.debug", "false");
			properties.put("mail.smtp.socketFactory.port", "465");
			properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			properties.put("mail.smtp.socketFactory.fallback", "false");

			Session session = Session.getDefaultInstance(properties,
					new javax.mail.Authenticator() {
						@Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication("team2scrum@gmail.com", "scrum4life");
						}
					});

			try {
				Transport transport = session.getTransport();

				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(from));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
				message.setSubject("Du har fått ny bruker i SKS2.0");
				message.setText("Ditt passord er: " + password);
				transport.connect(from, "scrum4life");
				Transport.send(message);
				transport.close();
				System.out.println("Sent message");
			} catch (MessagingException e) {
				e.printStackTrace(System.err);
			}

		}
	}

	/**
	 * Returns String text.
	 * Text is used as a String containing all the users that are added via addUsers().
	 * @return 
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the String text.
	 * Text is used as a String containing all the users that are added via addUsers().
	 * @param text 
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Returns String subject
	 * Subject is the selected subject for both addUser and addUsers as the subject field for the database.
	 * @return subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject field for the users.
	 * Subject is the selected subject for both addUser and addUsers as the subject field for the databse.
	 * @param subject 
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

}
