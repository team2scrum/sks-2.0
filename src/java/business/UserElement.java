package business;


public class UserElement {
	private String name;
	private final String email;
	private boolean hasGroup;
	
	public UserElement(String fname, String lname, String email) {
		name = fname + " " + lname;
		this.email = email;
	}
	
	public String getName(){
		return name;
	}
	
	public String getNameGroupFormated() {
		if(hasGroup) name += " (med gruppe)";
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setName(String newName){
		name = newName;
	}
	
	public void hasGroup(boolean b){
		hasGroup = b;
	}

}
