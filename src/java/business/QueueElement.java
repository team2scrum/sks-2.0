package business;

import java.util.ArrayList;
import spring.domain.User;

public class QueueElement {
	
	private ArrayList<UserElement> users;
	private String floor;
	private String table;
	private String placement;
	private String comment;
	private int[] exercises;
    private boolean isHelped;
    private int timeInQueue;
	
	
	/**
	 * Empty constructor.
	 */
	public QueueElement(){}

	/**
	 * Makes a new QueueElement, given:
	 * @param users
	 * @param floor
	 * @param table
	 * @param comment
	 * @param exercises 
	 */
	public QueueElement(ArrayList<UserElement> users, String floor, String table, String comment, int[] exercises) {
		this.users = users;
		this.placement = floor +", "+ table;
		this.comment = comment;
		this.exercises = exercises;
                this.isHelped = false;
                this.timeInQueue = 0;
				this.floor = floor;
	}
	
	/**
	 * Makes a new QueueElement, given:
	 * @param user
	 * @param floor
	 * @param table
	 * @param comment
	 * @param exercises 
	 */
	public QueueElement(UserElement user, String floor, String table, String comment, int[] exercises) {
		users = new ArrayList<UserElement>();
		users.add(user);
		this.floor = floor;
		this.table = table;
		this.placement = floor +", "+ table;
		this.comment = comment;
		this.exercises = exercises;
		this.isHelped = false;
		this.timeInQueue=0;
	}

	/**
	 * Makes a new QueueElement, given:
	 * @param user
	 * @param floor
	 * @param table
	 * @param comment
	 * @param exercises 
	 */
	public QueueElement(User user, String floor, String table, String comment, int[] exercises) {
		users = new ArrayList<UserElement>();
		users.add(new UserElement(user.getfName(), user.getlName(), user.getEmail()));
		this.floor = floor;
		this.table = table;
		this.placement = floor +", "+ table;
		this.comment = comment;
		this.exercises = exercises;
		this.isHelped = false;
		this.timeInQueue=0;
	}
        
	/**
	 * Returns time 
	 * @return 
	 */
	public int getTimeInQueue(){
		return timeInQueue;
	}

	/**
	 * Increment time by one in queue.
	 */
	public void incrementTimeInQueue(){
		timeInQueue+=1;
	}

	/**
	 * Returns if an user is getting help or not.
	 * @return 
	 */
	public boolean getIsHelped(){
		return isHelped;
	}

	/**
	 * Sets helping.
	 * @param isHelped 
	 */
	public void setIsHelped(boolean isHelped){
		this.isHelped = isHelped;
	}
	
	/**
	 * Adds a user to an queue element.
	 * @param user 
	 */
	public void addUser(User user){
		users.add(new UserElement(user.getfName(), user.getlName(), user.getEmail()));
	}
	
	/**
	 * Gets a user from this element
	 * @return 
	 */
	public UserElement getUser() {
		if (users.size() > 1) users.get(0).hasGroup(true);
		return users.get(0);
	}
	
	/**
	 * Gets all users from this element
	 * @return 
	 */
	public UserElement[] getAllUsers(){
		UserElement[] allusers = new UserElement[users.size()];
		return users.toArray(allusers);
	}

	/**
	 * Sets the users in this element
	 * @param users 
	 */
	public void setUsers(ArrayList<UserElement> users) {
		this.users = users;
	}

	/**
	 * Gets the placement.
	 * @return 
	 */
	public String getPlacement() {
		return placement;
	}

	/**
	 * Sets placement.
	 * @param placement 
	 */
	public void setPlacement(String placement) {
		this.placement = placement;
	}

	/**
	 * Gets comment.
	 * @return 
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets comment.
	 * @param comment 
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Gets an array of exercises.
	 * @return 
	 */
	public int[] getExercises() {
		return exercises;
	}

	/**
	 * Sets exercises.
	 * @param exercises 
	 */
	public void setExercises(int[] exercises) {
		this.exercises = exercises;
	}
	
	/**
	 * Checks is and element exists in this element.
	 * @param u
	 * @return 
	 */
	public boolean contains(UserElement u){
		return users.contains(u);
	}
	
	@Override
	public String toString() {
		UserElement u = users.get(0);
		return u.getName();
	}
	
	/**
	 * Returns if this element is a group of users.
	 * @return 
	 */
	public boolean isStudentGroup() {
		return users.size() > 1;
	}
	
	/**
	 * Returns the name of the student (or the first student, if this is QueueElement is a student group).
	 * If this a student group, the text "(med gruppe)" will be appended to the end of the string.
	 * Example: "Ola Jensen (med gruppe)"
	 * Example: "Nils Hansen"
	 * @return 
	 */
	public String webGetName() {
		return users.get(0).getName() + (isStudentGroup() ? " (med gruppe)" : "");
	}
	
	/**
	 * Returns a string containing all the relevant exercises for this QueueElement.
	 * The text "Øving" will be appended to the beginning of the returned string.
	 * Example: "Øving 1 2 3"
	 * @return 
	 */
	public String webGetExercises() {
		String ret = "Øving";
		for (int ex : exercises) ret += " " + ex;
		return ret;
	}
	
	/**
	 * Returns this comment.
	 * @return 
	 */
	public String webGetComment() {
		return comment;
	}
	
	/**
	 * Returns this placement.
	 * @return 
	 */
	public String webGetPlacement() {
		return placement;
	}

	/**
	 * Returns this floor.
	 * @return 
	 */
	public String getFloor() {
		return floor;
	}

	/**
	 * Sets this floor.
	 * @param floor 
	 */
	public void setFloor(String floor) {
		this.floor = floor;
	}

	/**
	 * Returns this table.
	 * @return 
	 */
	public String getTable() {
		return table;
	}

	/**
	 * Sets this table.
	 * @param table 
	 */
	public void setTable(String table) {
		this.table = table;
	}
	
	
	
}
