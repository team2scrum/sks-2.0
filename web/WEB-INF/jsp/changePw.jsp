
	<%@page import="business.Encryption"%>
	<%@page import="spring.domain.User"%>
	<%@page import="database.ValidateInfo"%>
	<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@page contentType="text/html" pageEncoding="UTF-8"%>
	<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
	<!DOCTYPE html>
	<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<c:url value="CSS/style.css"/>">
		<link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.css"/>">
		<link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.min.css"/>">
		<link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.css"/>">
		<link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.min.css"/>">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>SKS2.0</title>
	</head>
	<body>
		<div class="page-header centered">

			<h1 class="headertext">SKS 2.0 <small class="headertext">Høgskolen i Sør-Trøndelag</small></h1>

		</div>

		<div id="menu">
			<table>
				<f:form method="POST" modelAttribute="userInfo">

					<tr>
						<td>Du er nå logget inn som:</td>
					</tr>
					<tr>
						<td id="loginfo">
							<strong>${userInfo.fName} ${userInfo.lName}</strong>
							<br>
							(${userInfo.userTypeTitle})
						</td>
						<td></td>
					</tr>
					<tr>
						<td>
						</td>
					</tr>

					<c:if test="${(userInfo.getUser_type() <= 2) && (userInfo.getUser_type() != -1)}">
						<tr>
							<td>
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Godkjenningsliste</a>
							</td>
						</tr>
					</c:if>
					<c:if test="${(userInfo.getUser_type() == 3)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Øvingsoversikt</a>
							</td>
						</tr>
					</c:if>
					<tr>
						<td> 
							<a class="btn btn-standard" href="<s:url value="queue.htm"/>">Vis kø</a>
						</td>
					</tr> 

					<c:if test="${(userInfo.getUser_type() == 0) || (userInfo.getUser_type() == 1)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="addUser.htm"/>">Legg til bruker</a> 
							</td>
						</tr>
					</c:if>

					<tr>
						<td>
							<a class="btn btn-standard" href="<s:url value="changePw.htm"/>">Bytt passord</a>
						</td>
					</tr>



					<tr>
						<td>
							<a class="btn btn-standard" href="<s:url value="logout.htm"/>">Logg ut</a>
						</td>
					</tr>
				</table>
			</f:form>
		</div>


		<f:form form="POST" action="changePw.htm">
			<div id="password">
				<h1>Bytt passord</h1>
				<table id="changePWtable">
					<h3 class="subHeader">Passordet må være minst 8 tegn, og kan ikke være et enkelt ord.</h3>
					<tr>
						<td>Gammelt Passord:</td>
						<td><input type="password" class="pwinput" id="oldPw" name="oldPassword" placeholder="Gammelt Passord"/></td>
					</tr>
					<tr>
						<td>Nytt Passord:</td>
						<td><input type="password" class="pwinput" id="newPw1" name="newPassword1" placeholder="Nytt Passord"/></td>
					</tr>
					<tr>
						<td>Gjenta Passord:</td>
						<td><input type="password" class="pwinput" id="newPw2" name="newPassword2" placeholder="Gjenta Passord"/></td>
					</tr>
					<tr>
						<td><button class="btn btn-standard" id="changePWButton">Bytt passord</button></td>
					</tr>
				</table>

			</f:form>
			<table>
				<tr>
					<td>
						<%
							User user = (User) session.getAttribute("user");
							String newPw = request.getParameter("newPassword1");
							if (newPw != null) {
								Encryption enc = new Encryption();
								if (java.util.Arrays.equals(user.getCryptPass(), enc.encrypt(newPw))) {
									out.println("<div id='errortext_correct'>Passordet ble byttet! </div>");
								} else if (newPw.length() < 8) {
									out.println("<div id='errortext_correct'>Passordet er for kort, prøv igjen. </div>");
								} else if (ValidateInfo.isSimpleWord(newPw)) {
									out.println("<div id='errortext_correct'>Passordet er for enkelt, prøv igjen. </div>");
								} else {
									out.println("<div id='errortext_wronk'>Passordet ble ikke byttet!</div>");
								}
							}
						%>
					</td>
				</tr>
			</table>
		</div>
	</body>
	</html>
