<%-- 
    Document   : exerciseRequierments
    Created on : Jan 22, 2014, 3:27:38 PM
    Author     : Ruben
--%>

<%@page import="spring.domain.ExerciseGroup"%>
<%@page import="java.util.ArrayList"%>
<%@page import="database.RetrieveInfo"%>
<%@page import="spring.domain.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
		 <div class="page-header centered">

            <h1 class="headertext">SKS 2.0 <small class="headertext">Høgskolen i Sør-Trøndelag</small></h1>

        </div>

        <div id="menu">
            <table>
                <f:form method="POST" modelAttribute="userInfo">
					
                    <tr>
						<td>Du er nå logget inn som:</td>
					</tr>
					<tr>
						<td id="loginfo">
							<strong>${userInfo.fName} ${userInfo.lName}</strong>
							<br>
							(${userInfo.userTypeTitle})
							</td>
							<td></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>

					<c:if test="${(userInfo.getUser_type() <= 2) && (userInfo.getUser_type() != -1)}">
						<tr>
							<td>
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Godkjenningsliste</a>
							</td>
						</tr>
					</c:if>
					<c:if test="${(userInfo.getUser_type() == 3)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Øvingsoversikt</a>
							</td>
						</tr>
					</c:if>
                    <tr>
						<td> 
							<a class="btn btn-standard" href="<s:url value="queue.htm"/>">Vis kø</a>
						</td>
					</tr>

                    <c:if test="${(userInfo.getUser_type() == 0) || (userInfo.getUser_type() == 1)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="addUser.htm"/>">Legg til bruker</a> 
							</td>
						</tr>
                    </c:if>

					<tr>
						<td>
							<a class="btn btn-standard" href="<s:url value="changePw.htm"/>">Bytt passord</a>
						</td>
					</tr>

					

					<tr>
                        <td>
                            <a class="btn btn-standard" href="<s:url value="logout.htm"/>">Logg ut</a>
                        </td>
                    </tr>
				</table>
			</f:form>
		</div>
		
		<script type="text/javascript">
			function addExercises(){
				document.getElementById('changeSubjectCheck').value = "false";
				document.getElementById('requirementForm').submit();
			}
			
			function changeSubject(){
				document.getElementById('changeSubjectCheck').value = "true";
				document.getElementById('requirementForm').submit();
			}
		</script>
		<div id="addRequirements">
			<h4>Legg til arbeidskrav</h4>
			<form method="POST" action="exerciseRequirements.htm" id="requirementForm">
				<table>
					<tr>
						<td>Velg fag: </td>
							<td>
								<input type="hidden" value="false" id="changeSubjectCheck" name='changeSubjectCheck'/>
								<select onchange='changeSubject()' name='subjectSelected'>
								<%
									User user = (User) session.getAttribute("user");
									String[] subCode = RetrieveInfo.getSubjectCodes(user.getEmail());
									String[] subName = RetrieveInfo.getSubjects(user.getEmail());
									String selected = request.getParameter("subjectSelected");
									System.out.println(user.getEmail());
									for (int i = 0; i < subCode.length; i++) {
										if (selected != null && selected.equals(subCode[i])) {
											out.println("<option selected value='" + subCode[i] + "'>" + subName[i] + "</option>");
										} else {
											out.println("<option value='" + subCode[i] + "'>" + subName[i] + "</option>");
									}
								}
								%>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<select name="exercisesSelected" multiple="true">
								<%
									if(selected!=null){
										int numExercises = RetrieveInfo.getUserExercises(user.getEmail(), selected).size();
										int i = 1;
										while((i-1)<numExercises){
											out.println("<option value="+i+">"+i+"</option>");
											i++;
										}
									}
								%>
							</select>
							
						</td>
						<td>
							<ol>
								<%
									ArrayList<ExerciseGroup> exGrp = (ArrayList)session.getAttribute("exReq");
									if(exGrp!=null){
										for(ExerciseGroup aGroup : exGrp){
											if(aGroup.getSubjectCode().equals(selected)){
											out.println("<li>Gruppe: [");
											for(Integer anExercise : aGroup.getExercises()){
												out.println(anExercise+" ");
											}
											out.println("]</li>");
											}
										}
									}
								%>
							</ol>
						</td>
					</tr>
					<tr>
						<td>
							<button class="btn btn-standard" onchange="addExercises()">Legg til gruppe</button>
						</td>
					</tr>
				</table>
			</form>
		</div>
    </body>
</html>
