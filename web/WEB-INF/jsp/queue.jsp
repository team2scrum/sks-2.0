<%@page import="database.RetrieveInfo"%>
<%@page import="spring.domain.User"%>
<%@page import="business.Queue"%>
<%@page import="business.QueueElement"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!DOCTYPE html>


<html>

    <head>
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/style.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.min.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.min.css"/>">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		

        <title>SKS 2.0</title>

    </head>

    <body>

		<script type="text/javascript">
			var col = ['black', 'green', 'yellow', 'gray', 'blue', 'pink', 'purple', 'red'];

			function toggle(source) {
				checkboxes = document.getElementsByName('ovingcheck');
				var targetString = 'check' + source;
				target = document.getElementsByClassName(targetString);
				for (var i = 0; i < target.length; i++) {
					if (target[i].checked === true) {

					document.getElementById("godkjennButton").value = source;
					document.getElementById("godkjennButton").click();
					return;
					}
				}
				
				for (var i = 0, n = checkboxes.length; i < n; i++) {
					checkboxes[i].checked = false;
				}
				for (var i = 0, n = target.length; i < n; i++) {
					target[i].checked = true;
				}
				
				var trid = "tr"+source;
	
				trAll = document.getElementsByName('trName');
				for (var i = 0, n = trAll.length; i < n; i++) {
					trAll[i].style.display = "none";
				}
				document.getElementById(trid).style.display = "inline";
				
			}
			function printValue(a, b) {
				var c = document.getElementById(a).value;
				document.getElementById(b).value = c;
				//document.body.style.background = col[c];
			}

			


		</script>

		<div class="page-header centered">

            <h1 class="headertext">SKS 2.0 <small class="headertext">Høgskolen i Sør-Trøndelag</small></h1>

        </div>
		<div id="slider"><form name="sliderForm">
				<input type="range" id="inputSlider" min="1" max="10" step="1" onchange="printValue('inputSlider', 'sliderValue')">
				<input id="sliderValue" type="text" size="2"></form></div>


        <div id="menu">	
            <table>
                <f:form method="POST" modelAttribute="userInfo">

                    <tr>
						<td>Du er nå logget inn som:</td>
					</tr>
					<tr>
						<td id="loginfo">
							<strong>${userInfo.fName} ${userInfo.lName}</strong>
							<br>
							(${userInfo.userTypeTitle})
						</td>
						<td></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>

					<c:if test="${(userInfo.getUser_type() <= 2) && (userInfo.getUser_type() != -1)}">
						<tr>
							<td>
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Godkjenningsliste</a>
							</td>
						</tr>
					</c:if>
					<c:if test="${(userInfo.getUser_type() == 3)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Øvingsoversikt</a>
							</td>
						</tr>
					</c:if>
                    <tr>
						<td> 
							<a class="btn btn-standard" href="<s:url value="queue.htm"/>">Vis kø</a>
						</td>
					</tr>

                    <c:if test="${(userInfo.getUser_type() == 0) || (userInfo.getUser_type() == 1)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="addUser.htm"/>">Legg til bruker</a> 
							</td>
						</tr>
                    </c:if>

					<tr>
						<td>
							<a class="btn btn-standard" href="<s:url value="changePw.htm"/>">Bytt passord</a>
						</td>
					</tr>



					<tr>
                        <td>
                            <a class="btn btn-standard" href="<s:url value="logout.htm"/>">Logg ut</a>
                        </td>
                    </tr>
				</table>
			</f:form>
		</div>

		<div id="queueButtonDiv">
			<form method="POST" action="changeQueueState.htm" id="changeQueueState">
				<% 
				String queueButtons = (String) session.getAttribute("queueButtons");
				String getInQueue = (String) session.getAttribute("getInQueue");
				%>
				<%= queueButtons %>
			</form> 
			<form method="POST" action="getInQueue.htm">
				<%= getInQueue %>
			</form>
		</div>

		<div id="queueDiv"><form method="POST" action="changeDropdown.htm" id="dropDownQueueForm">
				
				<%
					User user = (User) session.getAttribute("user");
					String[] subjects = RetrieveInfo.getSubjects(user.getEmail());
					String selectedValue = (String) session.getAttribute("selectedValue");
					String hej = "";
					String[] subjectCode = new String[subjects.length];
					for (int i = 0; i < subjectCode.length; i++) {
						String[] temp = subjects[i].split("\\s+");
						subjectCode[i] = temp[0];
					}
				%>

				
				<select name="selectedValue" onchange='this.form.submit()'>
					

					<%						
					for (int i = 0; i < subjects.length; i++) {

							if (subjectCode[i].equals(selectedValue)) {
								hej = "<option selected=\"selected\" value=\"" + subjectCode[i] + "\">" + subjects[i] + "</option>";
							} else {
								hej = "<option value=\"" + subjectCode[i] + "\">" + subjects[i] + "</option>";
							}
							out.print(hej);

						}
						if (selectedValue == null || selectedValue.equals("")) {
							hej = "<option selected=\"selected\" value=\"-1\">--Velg fag--</option>";

						} else {
							hej = "<option value=\"-1\">--Velg fag--</option>";

						}
						out.print(hej);
					%> 



				</select>
			</form>
			<table class="table notSelect" id="queueTable">
				<colgroup>
					<col span="1">
					<col span="1">
					<col span="1">
					<col span="1">
					<col span="1">
				</colgroup>
				<thead>

					<tr>
						<th id="td1">Nummer</th>
						<th id="td2">Navn</th>
						<th id="td3">Øving</th>
						<th id="td4">Tid</th>
						<th id="td5">Actions</th>
					</tr>

				</thead>

				<tbody id="tBody">
                        <%
                            String queueTable = (String) request.getSession().getAttribute("queueTable");
                        %>
                        <f:form method="post" modelAttribute="qe" action="deleteTable.htm" id="springform">
                        
						
						<form>
                            <button id="godkjennButton" name="qe" style="display:none" value="0"></button>
                            <%= queueTable%>
                        </form>
                    </f:form>
                    </tbody></table>

		</div>
	</div>

</body>

</html>
