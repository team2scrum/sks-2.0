    <%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"

	"http://www.w3.org/TR/html4/loose.dtd">



<html>

    <head>
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/style.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.min.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.min.css"/>">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>SKS 2.0</title>

    </head>
		<body>

				<div class="page-header centered">
					<h1 class="headertext">SKS 2.0 <small class="headertext">Høgskolen i Sør-Trøndelag</small></h1>
				</div>


		<div id="loginlogo">
			<div id="histlogo">
				<a href="http://www.hist.no"/>
				<img src="CSS/histlogo.jpg" alt="hist" height="200" width="200"/>
				</a>
			</div>
			<div id="login">
				<h1>Logg inn</h1>
					<table id="loginTable">
						<f:form method="POST" modelAttribute="userInfo" action="checkLogin.htm">

						<h3 class="subHeader">Skriv inn brukernavn og passord</h3>
						
						<tr>
							<td>E-post</td>
						</tr>
						<tr>
							<td>
								<f:input class="userInput" path="email" required="required"/>
							</td>
							<td>
								<f:errors path="email"/>
							</td>
						</tr>
						<tr>
							<td>Passord</td>
						</tr>
						<tr>
							<td>
								<input class="userInput" type="password" name="password"/>
							</td>
							<td>
								
							</td>
						</tr>
						<tr>
							<td>
								<button class="btn btn-standard centered" type="submit">Logg inn</button>
								<br><a href="newPass.jsp">Glemt passord?</a>
							</td>
						</f:form>
						</tr>
				</table>
			</div>
		</div>


		<div id="footer">
			Team2, Høgskolen i Sør-Trøndelag - HiST© 
			<br>
			Brukerstøtte: 45410811 / team2scrum@gmail.com
		</div>
	</body>
</html>

