<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="spring.domain.User"%>
<%@page import="database.RetrieveInfo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/style.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.min.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.min.css"/>">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<link rel="shortcut icon" href="<c:url value="CSS/histicon.ico"/>">
        <title>SKS 2.0</title>
    </head>
    <body>
		<div class="page-header centered">

            <h1 class="headertext">SKS 2.0 <small class="headertext">Høgskolen i Sør-Trøndelag</small></h1>

        </div>

        <div id="menu">
            <table>
                <f:form method="POST" modelAttribute="userInfo">

                    <tr>
						<td>Du er nå logget inn som:</td>
					</tr>
					<tr>
						<td id="loginfo">
							<strong>${userInfo.fName} ${userInfo.lName}</strong>
							<br>
							(${userInfo.userTypeTitle})
						</td>
						<td></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>

					<c:if test="${(userInfo.getUser_type() <= 2) && (userInfo.getUser_type() != -1)}">
						<tr>
							<td>
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Godkjenningsliste</a>
							</td>
						</tr>
					</c:if>
					<c:if test="${(userInfo.getUser_type() == 3)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Øvingsoversikt</a>
							</td>
						</tr>
					</c:if>
                    <tr>
						<td> 
							<a class="btn btn-standard" href="<s:url value="queue.htm"/>">Vis kø</a>
						</td>
					</tr>

                    <c:if test="${(userInfo.getUser_type() == 0) || (userInfo.getUser_type() == 1)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="addUser.htm"/>">Legg til bruker</a> 
							</td>
						</tr>
                    </c:if>

					<tr>
						<td>
							<a class="btn btn-standard" href="<s:url value="changePw.htm"/>">Bytt passord</a>
						</td>
					</tr>



					<tr>
                        <td>
                            <a class="btn btn-standard" href="<s:url value="logout.htm"/>">Logg ut</a>
                        </td>
                    </tr>
				</table>
			</f:form>
		</div>

		<jsp:useBean id="userBean" scope="request" class="spring.domain.User"/>

		<div id="userDiv">
			<h1>Legg til ny bruker</h1>

			<f:form method="POST" modelAttribute="userText">


				<h3 class="subHeader" style="width:400px;">Legg til flere brukere samtidig</h3>
				<div id="separator"></div>
				<table>
					<tr>
						<td>
							<f:textarea path="text" items="${uc.text}" rows="20" cols="60"/> 
						</td>
					</tr>
					<tr>
						<td> 
							<f:select id="subjectDropdown" path="subject" items="${userSession.getSubjects()}" multiple="false">
							</f:select>
						</td>
					</tr>
					<tr>
						<td>
							<button class="btn btn-standard centered" type="submit">Lag brukere</button>
						</td>
					</tr>
				</table>	

			</f:form>

			<f:form method="POST" modelAttribute="userInfo">
				<div id="singleDiv">
					<table id="single">

						<tr>
						<h3 class="subHeader">Legg til én bruker</h3>
						<td>Fornavn</td>
						<td><f:input class="userInput" path="fName"/></td>
						</tr>

						<tr>
							<td>Etternavn</td>
							<td><f:input class="userInput" path="lName"/></td>
						</tr>

						<tr>
							<td>Epost</td>
							<td><f:input class="userInput" path="email"/></td>
						</tr>

						<tr>
							<td> 
								<f:select  id="subjectDropdown" path="subjects" items="${userSession.getSubjects()}" multiple="false">
								</f:select>
							</td>
						</tr>
						<c:if test="${(userSession.getUser_type()== 0) || (userSession.getUser_type()==1)}">
							<tr>
								<td>
									<f:select  path="user_type" multiple="false">
										<f:option value="1" label="Lærer"/>
										<f:option value="2" label="Studentassistent"/>
										<f:option value="3" label ="Student"/>			
									</f:select>
								</td>
							</tr>
						</c:if>
						<tr>
							<td>
								<button class="btn btn-standard centered" type="submit">Lag bruker</button>
							</td>
						</tr>
					</f:form>
				</table>
			</div>
		</div>
	</body>
</html>
