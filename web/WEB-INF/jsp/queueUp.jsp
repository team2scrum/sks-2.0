<%-- 
    Document   : queueUp
    Created on : Jan 20, 2014, 2:56:20 AM
    Author     : Ruben
--%>
<%@page trimDirectiveWhitespaces="true"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!DOCTYPE html>
<html>
    <head>
		<link rel="stylesheet" type="text/css" href="<c:url value="CSS/style.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.min.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.min.css"/>">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SKS 2.0</title>
    </head>
    <body>
		 <div class="page-header centered">

            <h1 class="headertext">SKS 2.0 <small class="headertext">Høgskolen i Sør-Trøndelag</small></h1>

        </div>

        <div id="menu">
            <table>
                <f:form method="POST" modelAttribute="userInfo">
					
                    <tr>
						<td>Du er nå logget inn som:</td>
					</tr>
					<tr>
						<td id="loginfo">
							<strong>${userInfo.fName} ${userInfo.lName}</strong>
							<br>
							(${userInfo.userTypeTitle})
							</td>
							<td></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>

					<c:if test="${(userInfo.getUser_type() <= 2) && (userInfo.getUser_type() != -1)}">
						<tr>
							<td>
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Godkjenningsliste</a>
							</td>
						</tr>
					</c:if>
					<c:if test="${(userInfo.getUser_type() == 3)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Øvingsoversikt</a>
							</td>
						</tr>
					</c:if>
                    <tr>
						<td> 
							<a class="btn btn-standard" href="<s:url value="queue.htm"/>">Vis kø</a>
						</td>
					</tr>

                    <c:if test="${(userInfo.getUser_type() == 0) || (userInfo.getUser_type() == 1)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="addUser.htm"/>">Legg til bruker</a> 
							</td>
						</tr>
                    </c:if>

					<tr>
						<td>
							<a class="btn btn-standard" href="<s:url value="changePw.htm"/>">Bytt passord</a>
						</td>
					</tr>
			
					

					<tr>
                        <td>
                            <a class="btn btn-standard" href="<s:url value="logout.htm"/>">Logg ut</a>
                        </td>
                    </tr>
				</table>
			</f:form>
		</div>
		
		<script type="text/javascript">
			window.onload = function(){
				var a ="";
				for(var i = 0;i<21;i++){
					a+="<option value='"+(i+1)+"'>"+(i+1)+"</option>";
				}
				document.getElementById("floorImage").style.backgroundImage = "url('CSS/polet.jpg')";
				document.getElementById("tableList").innerHTML = a;
			}
			
			
			
			function changeImageValue(){
				var e = document.getElementById("floorList");
				 var img = e.options[e.selectedIndex].value;
				 var tableAmount = 21;
				if(img==='1 etg. '){
					document.getElementById("floorImage").style.backgroundImage = "url('CSS/polet.jpg')";
				} else if(img==='2 etg. '){
					tableAmount = 14;
					document.getElementById("floorImage").style.backgroundImage = "url('CSS/lab2.jpg')";
				} else if(img==='4 etg. '){
					document.getElementById("floorImage").style.backgroundImage = "url('CSS/lab4.jpg')";
				}
				var a = "";
				for(var i = 0;i<tableAmount;i++){
					a+="<option value='"+(i+1)+"'>"+(i+1)+"</option>";
				}
				document.getElementById("tableList").innerHTML = a;
				
			}
		</script>
		<input type="hidden" value="1" id="imgSrc"/>
        <f:form method="POST" modelAttribute="queueElement" action="queueUp.htm">
			<table>
				<tr>
					<td>
						<%
						String subject = (String)session.getAttribute("selectedValue");
						out.println("<h4>Still deg i kø for faget "+subject+"</h4>");
						%>
					</td>
				</tr>
				<tr>
					<td>
						Hvor sitter du?
					</td>
					<td>
						<f:select path="floor" onchange="changeImageValue();" id="floorList">
							<f:option value="1 etg. " label="Polet, 1 etg." selected="selected"/>
							<f:option value="2 etg. " label="Labben, 2 etg."/>
							<f:option value="4 etg. " label="Nettlabben, 4 etg."/>
						</f:select>
					</td>
				</tr>
				<tr>
					<td>
						Bordnr
					</td>
					<td>
						<select name="table" id="tableList">
							
						</select>
					</td>
				</tr>
				<tr>
					<td>
						Øvinger
					</td>
					<td>
						<select name="exercises" multiple="true">
							<%
							ArrayList<Integer> exercises = (ArrayList)session.getAttribute("exerciseList");
							for(int exercise : exercises){
								out.println("<option value='"+exercise+"'>"+exercise+"</option>");
							}
							%>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						Kommentar
					</td>
					<td>
						<f:input path="comment" name="comment"/>
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="OK">
					</td>
					<td>
						<input type="reset" value="Reset">
					</td>
				</tr>
			</table>
		</f:form>
		<div id="floorImage" style="height: 500px; width: 1000px; background-repeat: no-repeat;">
			
		</div>
    </body>
</html>
