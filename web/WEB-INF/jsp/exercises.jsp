
<%@page trimDirectiveWhitespaces="true"%>
<%@page import="java.util.ArrayList"%>
<%@page import="database.RetrieveInfo"%>
<%@page import="spring.domain.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
		<link rel="stylesheet" type="text/css" href="<c:url value="CSS/style.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap-theme.min.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="CSS/bootstrap.min.css"/>">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>SKS 2.0</title>
    </head>
    <body>
		<script type="text/javascript">

			function selectSubject() {
				document.getElementById('buttonIsClicked').value = "false";
				document.getElementById('subjectForm').submit();
			}

			function buttonClicked() {
				alert("Downloading");
				document.getElementById('buttonIsClicked').value = "true";
				document.getElementById('subjectForm').submit();
			}
		</script>
		<div class="page-header centered">

            <h1 class="headertext">SKS 2.0 <small class="headertext">Høgskolen i Sør-Trøndelag</small></h1>

        </div>

        <div id="menu">
            <table>
                <f:form method="POST" modelAttribute="userInfo">

                    <tr>
						<td>Du er nå logget inn som:</td>
					</tr>
					<tr>
						<td id="loginfo">
							<strong>${userInfo.fName} ${userInfo.lName}</strong>
							<br>
							(${userInfo.userTypeTitle})
						</td>
						<td></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>

					<c:if test="${(userInfo.getUser_type() <= 2) && (userInfo.getUser_type() != -1)}">
						<tr>
							<td>
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Godkjenningsliste</a>
							</td>
						</tr>
					</c:if>
					<c:if test="${(userInfo.getUser_type() == 3)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="exercises.htm"/>">Øvingsoversikt</a>
							</td>
						</tr>
					</c:if>
                    <tr>
						<td> 
							<a class="btn btn-standard" href="<s:url value="queue.htm"/>">Vis kø</a>
						</td>
					</tr>

                    <c:if test="${(userInfo.getUser_type() == 0) || (userInfo.getUser_type() == 1)}">
						<tr>
							<td> 
								<a class="btn btn-standard" href="<s:url value="addUser.htm"/>">Legg til bruker</a> 
							</td>
						</tr>
                    </c:if>

					<tr>
						<td>
							<a class="btn btn-standard" href="<s:url value="changePw.htm"/>">Bytt passord</a>
						</td>
					</tr>



					<tr>
                        <td>
                            <a class="btn btn-standard" href="<s:url value="logout.htm"/>">Logg ut</a>
                        </td>
                    </tr>
				</table>
			</f:form>
		</div>

		<div id="exercises">
			<table id="exercisesTable">
				<col width="300">
				<col width="500">
				<tr>
					<%
						User user = (User) session.getAttribute("user");
						String[] subCode = RetrieveInfo.getSubjectCodes(user.getEmail());
						String[] subName = RetrieveInfo.getSubjects(user.getEmail());
						System.out.println(user.getEmail());
						//if(user.getUser_type()>2){
						out.println("<td style='border-bottom:1px solid #CCC;'>");
						out.println("<h4>Dine fag</h4>");
						for (int i = 0; i < subCode.length; i++) {
							out.println("<tr height='40px'><td id='tdSubject'>");
							ArrayList<Integer> approved = RetrieveInfo.getUserExercises(user.getEmail(), subCode[i]);
							//System.out.println(subCode[i]+" "+approved.size());
							if (RetrieveInfo.isReadyForExam(user.getEmail(), subCode[i])) {
								out.println("<span id='approved'>" + subName[i] + ":</span>");
							} else {
								out.println("<span>" + subName[i] + ":</span>");
							}
							String res = "</td><td>&nbsp;&nbsp;";

							for (int j = 1; j < approved.size() + 1; j++) {
								res += " ";
								if (approved.get(j - 1) == 1) {
									res += "<span id='approved'>" + j + "</span>";
								} else if (approved.get(j - 1) == 0) {
									res += "<span id='notApproved'>" + j + "</span>";
								}
							}
							out.println(res);
						}
						//} else if(user.getUser_type()<=1) {
					%>
					</td></tr>
			</table>
			<table>
				<tr>
					<td><h4>Fagoversikt</h4></td>
				</tr>
				<form method='POST' action='exercises.htm' id='subjectForm'>
					<tr><td><select onchange="selectSubject()" name='subjectSelected'>
								<%
									String selected = request.getParameter("subjectSelected");
									for (int i = 0; i < subCode.length; i++) {
										if (selected != null && selected.equals(subCode[i])) {
											out.println("<option selected value='" + subCode[i] + "'>" + subName[i] + "</option>");
										} else {
											out.println("<option value='" + subCode[i] + "'>" + subName[i] + "</option>");
										}
									}%>
						</td></tr>
					<tr><td>
							<input type='hidden' id='buttonIsClicked' name='buttonClicked' value='false'/>
						</td>
					</tr>
				</form>
				<%
					//}
				%>
				</tr>
				<col width="500">
				<tr>
					<td>
						<%						if (selected != null) {
								out.println("<b>Eksamenskandidater i faget: " + request.getParameter("subjectSelected") + "</b>");
							}
						%>
					</td>
				</tr>
				<tr>
					<td>
						<div id="mydiv">
							<c:forEach items="${userList.getUserList()}" var="user">
								<c:out value="${user}"/>
								<br>
							</c:forEach>
						</div>
					</td>
				</tr>
			</table>	

			<c:if test="${userInfo.getUser_type()<=1}">
				<!-- Skal sjekke helt øverst i stede, legger det å legge til arbeidskrav i en egen tab-->
			</c:if>
			<button class="btn btn-standard" onclick="buttonClicked()">Send</button>
		</div>

    </body>
</html>
